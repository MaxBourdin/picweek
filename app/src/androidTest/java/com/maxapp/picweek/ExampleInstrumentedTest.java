package com.maxapp.picweek;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.SearchView;
import android.view.View;

import com.maxapp.picweek.activity.MainActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.maxapp.picweek", appContext.getPackageName());
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);


    @Test
    public void ensureTextChangesWork() {

//        Espresso.pressBack();
        // Type text and then press the button.
        onView(withId(R.id.pagerArchivesButton)).perform(click());
//        onView(withId(R.id.themesArchivesSearchView)).perform(click());
//        onView(isAssignableFrom(SearchView.class)).perform(typeText("HELLO"), pressKey(KeyEvent.KEYCODE_ENTER));

//        onView(withId(R.id.themesArchivesSearchView))
//                .perform(typeText("HELLO"), closeSoftKeyboard());
//        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText("example"), pressKey(KeyEvent.KEYCODE_ENTER));
//        typeSearchViewText("Hello");
        onView(withId(R.id.themesArchivesSearchView)).perform(typeSearchViewText("HELLO"));

        // Check that the text was changed.
        onView(withId(R.id.themesArchivesSearchView)).check(matches(withText("HELLO")));


    }

    public static ViewAction typeSearchViewText(final String text){
        return new ViewAction(){
            @Override
            public Matcher<View> getConstraints() {
                //Ensure that only apply if it is a SearchView and if it is visible.
                return allOf(isDisplayed(), isAssignableFrom(SearchView.class));
            }

            @Override
            public String getDescription() {
                return "Change view text";
            }

            @Override
            public void perform(UiController uiController, View view) {
                ((SearchView) view).setQuery(text,false);
            }
        };
    }
}
