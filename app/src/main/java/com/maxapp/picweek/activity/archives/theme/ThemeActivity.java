package com.maxapp.picweek.activity.archives.theme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.TextView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.maxapp.picweek.firebasemodel.FirebaseTheme;
import com.maxapp.picweek.tools.ColorsTools;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ThemeActivity extends AppCompatActivity {

    @BindView(R.id.themeRecyclerView)
    ThemeRecyclerView themeRecyclerView;

    @BindView(R.id.backThemeButton)
    ImageButton backThemeButton;

    @BindView(R.id.themeNameTV)
    TextView themeNameTV;

    private ColorsTools colorsTools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theme);

        ButterKnife.bind(this);

        colorsTools = new ColorsTools(this);

        initViews();

        Intent mIntent = getIntent();
        String themeId = mIntent.getStringExtra("themeId");
//        Toast.makeText(getApplicationContext(), "Theme id : " + themeId, Toast.LENGTH_SHORT).show();

        FirebasePhoto.updateThemePhotos(themeId, themeRecyclerView);
        FirebaseTheme.updateThemeName(themeId, themeNameTV);
    }

    private void initViews(){
        colorsTools.setCurrentIconTabColorFilter(backThemeButton);
        ColorsTools.setIconTabBackgroundColor(backThemeButton);
    }

    @OnClick(R.id.backThemeButton)
    void backThemebuttonClickListener(){
//        this.onBackPressed();
        finish();
    }

    public void onBackPressed() {
        finish();
    }
}
