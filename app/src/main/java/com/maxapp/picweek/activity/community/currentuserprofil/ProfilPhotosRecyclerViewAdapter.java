package com.maxapp.picweek.activity.community.currentuserprofil;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maxapp.picweek.R;
import com.maxapp.picweek.model.Photo;

import java.util.ArrayList;

/**
 * Created by bourdin on 07/10/17
 */

public class ProfilPhotosRecyclerViewAdapter extends RecyclerView.Adapter<ProfilPhotosRecyclerViewHolder> {

    private ArrayList<Photo> photoAL;
    private ProfilPhotosRecyclerViewHolder.ProfilPhotosRecyclerViewClickListener clickListener;

    public ProfilPhotosRecyclerViewAdapter(ArrayList<Photo> photoAL) {
        this.photoAL = photoAL;
//        notifyDataSetChanged();
    }

    public void update(ArrayList<Photo> photoAL) {
        //On change uniquement si il y a une nouvelle photo
        if(this.photoAL.size() != photoAL.size()){
            this.photoAL = photoAL;
            notifyDataSetChanged();
        }
    }

    @Override
    public ProfilPhotosRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_profil_photo,viewGroup,false);
        return new ProfilPhotosRecyclerViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(ProfilPhotosRecyclerViewHolder myViewHolder, int position) {
        Photo photo = photoAL.get(position);
        myViewHolder.bind(photo, position);
    }

    @Override
    public int getItemCount() {
        return photoAL.size();
    }

    public void setClickListener(ProfilPhotosRecyclerViewHolder.ProfilPhotosRecyclerViewClickListener clickListener){
        this.clickListener = clickListener;
    }

    public ArrayList<Photo> getPhotoAL() {
        return photoAL;
    }
}
