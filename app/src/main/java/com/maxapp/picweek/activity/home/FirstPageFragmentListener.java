package com.maxapp.picweek.activity.home;

/**
 * Created by bourdin on 22/11/17
 */

public interface FirstPageFragmentListener {
    void onSwitchToNextFragment();
}
