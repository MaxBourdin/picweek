package com.maxapp.picweek.activity.archives.theme;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.maxapp.picweek.R;
import com.maxapp.picweek.model.Photo;


/**
 * Created by bourdin on 07/10/17
 */

public class ThemeRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public interface RecyclerViewClickListener {
        void onClick(View view, int photoId);
    }

    private ImageView photoIV;
    private RecyclerViewClickListener clickListener;
    private int position;

    public ThemeRecyclerViewHolder(View itemView, RecyclerViewClickListener clickListener) {
        super(itemView);

        photoIV = itemView.findViewById(R.id.theme_photo_iv);

        itemView.setOnClickListener(this);
        this.clickListener = clickListener;
    }

    public void bind(final Photo photo, int position){
        Glide.with(itemView.getContext()).load(photo.getUrl()).into(photoIV);
        this.position = position;
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, this.position);
    }

}
