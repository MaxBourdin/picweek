package com.maxapp.picweek.activity.archives.theme;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.maxapp.picweek.model.Photo;
import com.maxapp.picweek.tools.FrescoImageViewerHandler;

import java.util.ArrayList;

/**
 * Created by bourdin on 18/11/17
 */

public class ThemeRecyclerView extends RecyclerView {
    private ThemeRecyclerViewAdapter themeRecyclerViewAdapter;
    private ArrayList<Photo> themePhotosAl = new ArrayList<>();

    public ThemeRecyclerView(Context context) {
        super(context);
        init();
    }
    public ThemeRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public ThemeRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        setLayoutManager(new LinearLayoutManager(getContext()));

        themePhotosAl = new ArrayList<>();

        themeRecyclerViewAdapter = new ThemeRecyclerViewAdapter(themePhotosAl);

        setAdapter(themeRecyclerViewAdapter);

        themeRecyclerViewAdapter.setClickListener(itemClickListener);
    }

    public void redraw(ArrayList<Photo> photos){
//        themeRecyclerViewAdapter = new ThemeRecyclerViewAdapter(photos);
//        setAdapter(themeRecyclerViewAdapter);
//        themeRecyclerViewAdapter.setClickListener(itemClickListener);
        themeRecyclerViewAdapter.update(photos);
    }


    ThemeRecyclerViewHolder.RecyclerViewClickListener itemClickListener = new ThemeRecyclerViewHolder.RecyclerViewClickListener() {
        @Override
        public void onClick(View view, int startPosition) {
            FrescoImageViewerHandler.showArchivesImages(getContext(), themeRecyclerViewAdapter.getPhotosList(), startPosition);
        }
    };

    public ThemeRecyclerViewAdapter getThemeRecyclerViewAdapter() {
        return themeRecyclerViewAdapter;
    }
}


