package com.maxapp.picweek.activity.community;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.maxapp.picweek.R;
import com.maxapp.picweek.activity.community.classement.ClassementFragment;
import com.maxapp.picweek.activity.community.currentuserprofil.ProfilFragment;
import com.maxapp.picweek.tools.ColorsTools;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommunityFragment extends Fragment {

    @BindView(R.id.pagerProfilButton) Button profilButton;
    @BindView(R.id.pagerClassementButton) Button classementButton;

    @BindView(R.id.communityPager) ViewPager communityPager;

    MyPagerAdapter adapter;

    private ColorsTools colorsTools;

    public CommunityFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static CommunityFragment newInstance() {
        CommunityFragment fragment = new CommunityFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_community, container, false);
        ButterKnife.bind(this, view);
        colorsTools = new ColorsTools(getContext());
        initViews();
        return view;
    }

    private void initViews(){
        adapter = new MyPagerAdapter(getChildFragmentManager());
        communityPager.setAdapter(adapter);
        communityPager.addOnPageChangeListener(onPageChangeListener);
        communityPager.setCurrentItem(0);
        changeButtonColor();
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            changeButtonColor();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void changeButtonColor(){
        switch (communityPager.getCurrentItem()){
            case 0 :
                colorsTools.setCurrentCommunityButtonTabBackgroundColor(profilButton);
                colorsTools.setCommunityButtonTabBackgroundColor(classementButton);
                break;
            case 1 :
                colorsTools.setCommunityButtonTabBackgroundColor(profilButton);
                colorsTools.setCurrentCommunityButtonTabBackgroundColor(classementButton);
                break;
        }
    }

    @OnClick(R.id.pagerProfilButton)
    void profilButtonClickListener(){
        communityPager.setCurrentItem(0);
    }

    @OnClick(R.id.pagerClassementButton)
    void classementButtonClickListener(){
        communityPager.setCurrentItem(1);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"Profil" , "Classement"};

        MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 : return ProfilFragment.newInstance();
                case 1 : return ClassementFragment.newInstance();
            }

            //n'arrive jamais
            return ProfilFragment.newInstance();
        }
    }
}
