package com.maxapp.picweek.activity.community.classement;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.maxapp.picweek.activity.profil.ProfilActivity;
import com.maxapp.picweek.model.Classement;
import com.maxapp.picweek.model.User;

import java.util.ArrayList;

/**
 * Created by bourdin on 18/11/17
 */

public class ClassementRecyclerView extends RecyclerView {
    private ClassementRecyclerViewAdapter recyclerViewAdapter;
    private Context context;

    public ClassementRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        setNestedScrollingEnabled(false);

        setLayoutManager(new LinearLayoutManager(context));
    }

    public void init(ArrayList<User> users){
//        ArrayList<User> userAL = new ArrayList<>();

        //Le bug du onclick viens du fait que la liste de user
        //n'est pas encore remplis à ce moment je pense
        recyclerViewAdapter = new ClassementRecyclerViewAdapter(users);

        setAdapter(recyclerViewAdapter);

        addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.VERTICAL));

        recyclerViewAdapter.setClickListener(new ClassementRecyclerViewHolder.RecyclerViewClickListener() {
            @Override
            public void onClick(View view, String userId) {
                Intent intent = new Intent(getContext(), ProfilActivity.class);
                intent.putExtra("userId", userId);
                getContext().startActivity(intent);
            }
        });
    }

    public void updateAdapter(Classement classement){
        recyclerViewAdapter = new ClassementRecyclerViewAdapter(classement.getUsersAL());
        setAdapter(recyclerViewAdapter);
    }

    public ClassementRecyclerViewAdapter getRecyclerViewAdapter() {
        return recyclerViewAdapter;
    }
}


