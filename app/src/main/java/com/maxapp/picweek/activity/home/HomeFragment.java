package com.maxapp.picweek.activity.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.activity.MainActivity;
import com.maxapp.picweek.dialog.ImportPhotoDialog;
import com.maxapp.picweek.firebasemodel.FirebaseTheme;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class HomeFragment extends Fragment  implements FirstPageFragmentListener{

    static FirstPageFragmentListener firstPageListener;

//    @BindView(R.id.homeButtonVote)Button homeButtonVote;
//    @BindView(R.id.homeButtonPost)ImageButton homeButtonPost;
    @BindView(R.id.voteIV)ImageView voteIV;
    @BindView(R.id.postIV)ImageView postIV;

    @BindView(R.id.voteTV)TextView voteTV;
    @BindView(R.id.postTV)TextView postTV;

    public HomeFragment() {
        // Required empty public constructor
    }

    public HomeFragment(FirstPageFragmentListener listener) {
        firstPageListener = listener;
    }

    public static HomeFragment newInstance(FirstPageFragmentListener listener) {
        return new HomeFragment(listener);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
//        Blurry.with(getContext())
//                .capture(view.findViewById(R.id.voteIV))
//                .into((ImageView) view.findViewById(R.id.voteIV));


        FirebaseTheme.updatePostAndVoteThemeFromHomeFragment(getContext(), voteIV, postIV, voteTV, postTV);


//        Glide.with(getContext()).load("https://fr.cdn.v5.futura-sciences.com/buildsv6/images/wide1920/9/3/2/9321f807a3_100799_soleil-rouge.jpg").into(voteIV);

//        voteIV.post(new Runnable() {
//            @Override
//            public void run() {
//                Blurry.with(getContext())
//                        .radius(6)
////                        .sampling(1)
//                        .color(Color.argb(70, 0, 0, 0))
//                        .async()
//                        .animate(0)
//                        .capture(voteIV)
//                        .into(voteIV);
//
//                Blurry.with(getContext())
//                        .radius(6)
//                        .color(Color.argb(70, 0, 0, 0))
//                        .async()
//                        .animate(0)
//                        .capture(postIV)
//                        .into(postIV);
//            }
//        });
//        homeButtonVote.setBackgroundResource(R.drawable.coucher_soleil);
        initViews();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }

    private void initViews(){

    }

    @OnClick(R.id.homeRelativeLayoutVote)
    void voteButtonClick(){
        firstPageListener.onSwitchToNextFragment();
    }

    @OnClick(R.id.homeRelativeLayoutPost)
    void postButtonClick(){
//        FirebasePhoto.writeNewPhoto("https://occ-0-2433-1001.1.nflxso.net/art/5973f/cf832ad9935510d0fa8dd6970398061d6705973f.jpg");
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED   ) {
            askForPermission();
        }else{
            ((MainActivity) getActivity()).setImportPhotoDialog(new ImportPhotoDialog(getContext()));
        }
    }

    private void askForPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 2);
        //OnRequestPermissionResult dans Main Activity
    }


    @Override
    public void onSwitchToNextFragment() {

    }
}
