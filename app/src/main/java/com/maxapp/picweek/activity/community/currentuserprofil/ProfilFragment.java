package com.maxapp.picweek.activity.community.currentuserprofil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.maxapp.picweek.firebasemodel.MyFirebaseUser;
import com.maxapp.picweek.tools.ColorsTools;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfilFragment extends Fragment {

    @BindView(R.id.communityPseudoTV) TextView pseudoTV;
    @BindView(R.id.communityNbLikesTV) TextView nbLikesTV;
    @BindView(R.id.communityNbPhotosTV) TextView nbPhotosTV;
    @BindView(R.id.likeImage) ImageView likeIV;
    @BindView(R.id.cameraImage) ImageView cameraIV;
    @BindView(R.id.profilAvatar) ImageView profilAvatar;
    @BindView(R.id.profilPhotosRecyclerView) ProfilPhotosRecyclerView profilPhotosRecyclerView;
    public static ProfilFragment fragment;
//    private boolean isInstanciate = false;

    private ColorsTools colorsTools;

    public ProfilFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static ProfilFragment newInstance() {
        fragment = new ProfilFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profil, container, false);
        ButterKnife.bind(this, view);
        this.colorsTools = new ColorsTools(getContext());
        initViews();
//        isInstanciate = true;
        return view;
    }

    private void initViews(){
//        User user = new User(MyFirebaseUser.getFirebaseUserUUID(), MyFirebaseUser.getName(), MyFirebaseUser.getLargeProfilePhotoURL(MyFirebaseUser.getCurrentFacebookUserId()));
//        UserController userController = new UserController(user, pseudoTV, nbPhotosTV, nbLikesTV, profilAvatar);
//        userController.updateName(user.getPseudo());
//        userController.updateProfilAvatar(user.getPhotoURL(), getContext());
//        FirebasePhoto.updatePhotosAndLikesCountForCurrentUser(userController);

        MyFirebaseUser.updateProfilInformations(MyFirebaseUser.getFirebaseUserUUID(), pseudoTV, profilAvatar);
        FirebasePhoto.updatePhotosAndLikesCountForUser(MyFirebaseUser.getFirebaseUserUUID(), nbLikesTV, nbPhotosTV);

        colorsTools.setCommunityIconColor(likeIV);
        colorsTools.setCommunityIconColor(cameraIV);
    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        System.out.println("teston " + isVisibleToUser + " " + isInstanciate);
//
//        if (isVisibleToUser && isInstanciate) {
//            System.out.println("teston we are here");
//            profilPhotosRecyclerView.updatePhotos();
//        }
//        else {
//        }
//    }

    public ProfilPhotosRecyclerView getProfilPhotosRecyclerView() {
        return profilPhotosRecyclerView;
    }
}
