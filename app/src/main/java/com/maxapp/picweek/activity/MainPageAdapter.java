package com.maxapp.picweek.activity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.maxapp.picweek.activity.archives.ThemesArchivesFragment;
import com.maxapp.picweek.activity.community.CommunityFragment;
import com.maxapp.picweek.activity.home.FirstPageFragmentListener;
import com.maxapp.picweek.activity.home.HomeFragment;
import com.maxapp.picweek.activity.home.VoteFragment;

/**
 * Created by bourdin on 22/11/17
 */

public class MainPageAdapter extends FragmentPagerAdapter {

    private final FragmentManager mFragmentManager;
    public Fragment mFragmentAtPos0;
    private Context context;
    FirstPageListener listener = new FirstPageListener();

    private final class FirstPageListener implements
            FirstPageFragmentListener {
        public void onSwitchToNextFragment() {
            mFragmentManager.beginTransaction().remove(mFragmentAtPos0)
                    .commitNow();
            if (mFragmentAtPos0 instanceof HomeFragment){
                mFragmentAtPos0 = VoteFragment.newInstance(listener);
            }else{ // Instance of NextFragment
                mFragmentAtPos0 = HomeFragment.newInstance(listener);
            }
            notifyDataSetChanged();
        }
    }

    private final String[] TITLES = {"Archives" , "Accueil", "Classement"};

    MainPageAdapter(FragmentManager fm) {
        super(fm);
        mFragmentManager = fm;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return ThemesArchivesFragment.newInstance();

            case 1 :
//                    return HomeFragment.newInstance();
                if (mFragmentAtPos0 == null)
                {
                    mFragmentAtPos0 = HomeFragment.newInstance(listener);
                }
                return mFragmentAtPos0;
            case 2 :
                return CommunityFragment.newInstance();
        }
        //n'arrive jamais
        return HomeFragment.newInstance(listener);
    }

    @Override
    public int getItemPosition(Object object)
    {
        if (object instanceof HomeFragment &&
                mFragmentAtPos0 instanceof VoteFragment) {
            return POSITION_NONE;
        }
        if (object instanceof VoteFragment &&
                mFragmentAtPos0 instanceof HomeFragment) {
            return POSITION_NONE;
        }
        return POSITION_UNCHANGED;
    }
}