package com.maxapp.picweek.activity.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.mindorks.placeholderview.SwipeDecor;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class VoteFragment extends Fragment {

    static FirstPageFragmentListener firstPageListener;

    @BindView(R.id.swipeView)
    public SwipePlaceHolderView mSwipeView;

    public VoteFragment() {
        // Required empty public constructor
    }

    public VoteFragment(FirstPageFragmentListener listener) {
        firstPageListener = listener;
    }

    public static VoteFragment newInstance(FirstPageFragmentListener listener) {
        return new VoteFragment(listener);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vote, container, false);
        ButterKnife.bind(this, view);

        mSwipeView.getBuilder()
                .setDisplayViewCount(3)
                .setSwipeDecor(new SwipeDecor()
                        .setPaddingTop(0)
                        .setRelativeScale(0.09f)
                        .setSwipeInMsgLayoutId(R.layout.swipe_in_message_view)
                        .setSwipeOutMsgLayoutId(R.layout.swipe_out_message_view));


        FirebasePhoto.updateVotePhotos(getActivity(), getContext(), mSwipeView);

        return view;
    }

//    @OnClick(R.id.rejectBtn)
//    void rejectButtonClick(){
//        mSwipeView.doSwipe(false);
//    }
//
//    @OnClick(R.id.acceptBtn)
//    void acceptButtonClick(){
//        mSwipeView.doSwipe(true);
//    }

    public void backPressed() {
        firstPageListener.onSwitchToNextFragment();
    }
}