package com.maxapp.picweek.activity.community.classement;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maxapp.picweek.R;
import com.maxapp.picweek.model.User;

import java.util.ArrayList;

/**
 * Created by bourdin on 07/10/17
 */

public class ClassementRecyclerViewAdapter extends RecyclerView.Adapter<ClassementRecyclerViewHolder> {

    private ArrayList<User> usersAL;
    private ClassementRecyclerViewHolder.RecyclerViewClickListener clickListener;

    //Constructeur avec en parametre notre liste de Tweets
    public ClassementRecyclerViewAdapter(ArrayList<User> usersAL) {
        this.usersAL = usersAL;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même occasion indiquer la vue à inflater (à partir des layout xml)
    @Override
    public ClassementRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_classement,viewGroup,false);
        return new ClassementRecyclerViewHolder(view, clickListener);
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque Tweet
    @Override
    public void onBindViewHolder(ClassementRecyclerViewHolder myViewHolder, int position) {
        User user = usersAL.get(position);
        myViewHolder.bind(user, position);
    }

    @Override
    public int getItemCount() {
        return usersAL.size();
    }

    public void setClickListener(ClassementRecyclerViewHolder.RecyclerViewClickListener clickListener){
        this.clickListener = clickListener;
    }
}
