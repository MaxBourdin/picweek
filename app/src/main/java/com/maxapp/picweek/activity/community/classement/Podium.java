package com.maxapp.picweek.activity.community.classement;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.maxapp.picweek.model.User;

/**
 * Created by bourdin on 20/11/17
 */

public class Podium{
    private Context context;
    private UserPodium firstUserPodium, secondUserPodium, thirdUserPodium;
    private LinearLayout mainLinearLayout;

    public Podium(Context context, User firstUserPodium, User secondUserPodium, User thirdUserPodium){
        this.context = context;

        this.firstUserPodium = new UserPodium(context, firstUserPodium, 1);
        this.secondUserPodium = new UserPodium(context, secondUserPodium, 2);
        this.thirdUserPodium = new UserPodium(context, thirdUserPodium, 3);

        mainLinearLayout = buildPodiumLinearLayout();

        mainLinearLayout.addView(this.secondUserPodium.getRelativeLayout());
        mainLinearLayout.addView(this.firstUserPodium.getRelativeLayout());
        mainLinearLayout.addView(this.thirdUserPodium.getRelativeLayout());
    }

    public void update(User firstUser, User secondUser, User thirdUser){
//        this.firstUserPodium = new UserPodium(context, firstUser, 1);
//        this.secondUserPodium = new UserPodium(context, secondUser, 2);
//        this.thirdUserPodium = new UserPodium(context, thirdUser, 3);

        this.firstUserPodium.updateTextAndImage(firstUser,1);
        this.secondUserPodium.updateTextAndImage(secondUser,2);
        this.thirdUserPodium.updateTextAndImage(thirdUser,3);
    }

    private LinearLayout buildPodiumLinearLayout(){
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setWeightSum(1.0f);
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

        return linearLayout;
    }

    public LinearLayout getMainLinearLayout() {
        return mainLinearLayout;
    }
}
