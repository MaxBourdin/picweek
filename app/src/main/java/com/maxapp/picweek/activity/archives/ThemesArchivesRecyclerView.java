package com.maxapp.picweek.activity.archives;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;

import com.maxapp.picweek.activity.archives.theme.ThemeActivity;
import com.maxapp.picweek.firebasemodel.FirebaseTheme;
import com.maxapp.picweek.model.Theme;

import java.util.ArrayList;

/**
 * Created by bourdin on 18/11/17
 */

public class ThemesArchivesRecyclerView extends RecyclerView {
    private ArchivesRecyclerViewAdapter archivesRecyclerViewAdapter;

    public ThemesArchivesRecyclerView(Context context) {
        super(context);
        init();
    }
    public ThemesArchivesRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public ThemesArchivesRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init(){
        setLayoutManager(new LinearLayoutManager(getContext()));

        ArrayList<Theme> themeAL = new ArrayList<>();
        archivesRecyclerViewAdapter = new ArchivesRecyclerViewAdapter(themeAL);

        FirebaseTheme.updateThemesList(archivesRecyclerViewAdapter);

        setAdapter(archivesRecyclerViewAdapter);

        addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        archivesRecyclerViewAdapter.setClickListener(itemClickListener);
    }

    RecyclerViewHolder.RecyclerViewClickListener itemClickListener = new RecyclerViewHolder.RecyclerViewClickListener() {
        @Override
        public void onClick(View view, String themeId) {
//            Toast.makeText(getContext(), themeId, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getContext(), ThemeActivity.class);
            intent.putExtra("themeId", themeId);
            getContext().startActivity(intent);
        }
    };

    public ArchivesRecyclerViewAdapter getArchivesRecyclerViewAdapter() {
        return archivesRecyclerViewAdapter;
    }
}


