package com.maxapp.picweek.activity.archives.theme;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.activity.profil.ProfilActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bourdin on 09/01/18
 */

public class OverlayView extends RelativeLayout {

    @BindView(R.id.overlayUserPseudo)TextView userPseudoTV;
    @BindView(R.id.overlayNbLikes)TextView nbLikesTV;
    @BindView(R.id.overlayShareButton)ImageView shareButton;
    @BindView(R.id.overlayReportButton)ImageView reportButton;

    private String userPseudo;
    private int nbLikes;
    private String userId;
    private String sharingText;
//    private ColorsTools colorsTools;
    private Context context;

    public OverlayView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public OverlayView(Context context, String userPseudo, int nbLikes) {
        super(context);
        this.context = context;
        this.userPseudo = userPseudo;
        this.nbLikes = nbLikes;
        init();
    }

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public OverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    public void setUserPseudo(String userPseudo){
        this.userPseudo = userPseudo;
        userPseudoTV.setText(userPseudo);
    }

    public void setNbLikes(int nbLikes){
        this.nbLikes = nbLikes;
        nbLikesTV.setText(nbLikes + " J'AIMES");
    }

    public void setUserId(String userId){
        this.userId = userId;
    }

    private void sendShareIntent() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, sharingText);
        sendIntent.setType("text/plain");
        getContext().startActivity(sendIntent);
    }

    private void startConsultProfilActivity(){
        Intent intent = new Intent(context, ProfilActivity.class);
        intent.putExtra("userId", userId);
        context.startActivity(intent);

        //TODO permet d'éviter les heaps dump (peut etre)
        ((ThemeActivity) getContext()).finish();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.overlay_view, this);
        ButterKnife.bind(this, view);
//        this.colorsTools = new ColorsTools(context);

        userPseudoTV.setText(userPseudo);
        nbLikesTV.setText(nbLikes + " J'AIMES");
    }

    @OnClick(R.id.overlayShareButton)
    void voteButtonClick(){
        sendShareIntent();
    }

    @OnClick(R.id.overlayUserPseudo)
    void shareButtonClick(){
        startConsultProfilActivity();
    }
}
