package com.maxapp.picweek.activity.profil;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;

import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.maxapp.picweek.model.Photo;
import com.maxapp.picweek.tools.FrescoImageViewerHandler;

import java.util.ArrayList;

/**
 * Created by bourdin on 18/11/17
 */

public class ConsultProfilRecyclerView extends RecyclerView {
    private ConsultProfilAdapter recyclerViewAdapter;
    private Context context;


    public ConsultProfilRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        setNestedScrollingEnabled(false);

        StaggeredGridLayoutManager gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        setLayoutManager(gaggeredGridLayoutManager);

        recyclerViewAdapter = new ConsultProfilAdapter(new ArrayList<Photo>());

        setAdapter(recyclerViewAdapter);
        recyclerViewAdapter.setClickListener(itemClickListener);
    }

    public void updatePhotos(String userId){
        FirebasePhoto.updatePhotosForUser(userId, this);
    }

    public void redraw(ArrayList<Photo> photos){
//        recyclerViewAdapter = new ConsultProfilAdapter(photos);
//        setAdapter(recyclerViewAdapter);
//        recyclerViewAdapter.setClickListener(itemClickListener);
        recyclerViewAdapter.update(photos);
    }

    ConsultProfilHolder.ConsultProfilClickListener itemClickListener = new ConsultProfilHolder.ConsultProfilClickListener() {
        @Override
        public void onClick(View view, int startPosition) {
//            Toast.makeText(context, "Name : " + name, Toast.LENGTH_SHORT).show();
            FrescoImageViewerHandler.showCurrentProfilImages(getContext(), recyclerViewAdapter.getPhotoAL(), startPosition);

        }
    };

    public ConsultProfilAdapter getRecyclerViewAdapter() {
        return recyclerViewAdapter;
    }
}


