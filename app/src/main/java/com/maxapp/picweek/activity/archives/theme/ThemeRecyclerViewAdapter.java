package com.maxapp.picweek.activity.archives.theme;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.maxapp.picweek.R;
import com.maxapp.picweek.model.Photo;

import java.util.ArrayList;

/**
 * Created by bourdin on 07/10/17
 */

public class ThemeRecyclerViewAdapter extends RecyclerView.Adapter<ThemeRecyclerViewHolder> {

    private ArrayList<Photo> photosList;
    private ThemeRecyclerViewHolder.RecyclerViewClickListener clickListener;

    public ThemeRecyclerViewAdapter(ArrayList<Photo> photosList) {
        this.photosList = photosList;
    }

    public void update(ArrayList<Photo> photosList){
        if(this.photosList.size() !=  photosList.size()){
            this.photosList = photosList;
            notifyDataSetChanged();
        }
    }

    @Override
    public ThemeRecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_theme_photo,viewGroup,false);
        return new ThemeRecyclerViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(ThemeRecyclerViewHolder myViewHolder, int position) {
        Photo photo = photosList.get(position);
        myViewHolder.bind(photo, position);
    }

    @Override
    public int getItemCount() {
        return photosList.size();
    }

    public void setClickListener(ThemeRecyclerViewHolder.RecyclerViewClickListener clickListener){
        this.clickListener = clickListener;
    }

    public ArrayList<Photo> getPhotosList() {
        return photosList;
    }
}
