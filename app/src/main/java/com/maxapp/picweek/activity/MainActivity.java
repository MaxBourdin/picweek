package com.maxapp.picweek.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;

import com.maxapp.picweek.R;
import com.maxapp.picweek.activity.home.HomeFragment;
import com.maxapp.picweek.activity.home.VoteFragment;
import com.maxapp.picweek.activity.login.LoginActivity;
import com.maxapp.picweek.dialog.ImportPhotoDialog;
import com.maxapp.picweek.firebasemodel.FirebaseTheme;
import com.maxapp.picweek.firebasemodel.MyFirebaseStorage;
import com.maxapp.picweek.tools.ColorsTools;
import com.maxapp.picweek.tools.FirebaseHandler;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 123;

    @BindView(R.id.pagerArchivesButton)ImageButton archivesButton;
    @BindView(R.id.pagerHomeButton)ImageButton homeButton;
    @BindView(R.id.pagerCommunityButton) ImageButton communityButton;

    @BindView(R.id.pager) CustomViewPager pager;

    private MainPageAdapter adapter;

    private ColorsTools colorsTools;

    private ImportPhotoDialog importPhotoDialog;

    public static MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        ButterKnife.bind(this);

        colorsTools = new ColorsTools(this);
        initViews();

//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.example.packagename",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }


//        FirebasePhoto.writeNewTheme("123", "https://i.pinimg.com/736x/35/b9/4f/35b94fe8085c76445d7d9a4820d57c32--amazing-sunsets-parcs.jpg","123456");
//        FirebasePhoto.writeNewTheme(MyFirebaseUser.getFirebaseUserUUID(), "https://i.pinimg.com/736x/cd/f3/65/cdf3656db558be59abfcacff8e803008--sunset-lover-beautiful-sunset.jpg","123456");

        Date dated1 = null;
        Date dated2 = null;
        Date dated3 = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        String date1 = "06/02/2018";
        String date2 = "15/01/2018";
        String date3 = "29/01/2018";
        try {
            dated1 = simpleDateFormat.parse(date1);
            dated2 = simpleDateFormat.parse(date2);
            dated3 = simpleDateFormat.parse(date3);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//
//        FirebaseTheme.writeNewTheme("test", dated3.getTime());
//        FirebaseTheme.writeNewTheme("Deuxieme voter", dated2.getTime());
//        FirebaseTheme.writeNewTheme("Troisieme poster", dated3.getTime());

//        FirebasePhoto.writeNewPhoto("0UiWrxAhlwUJbX01zfsnPc2q5cG3",  "https://media.koreus.com/201702/coucher-soleil-butterfly-valley.jpg", "-L3nJezfRjN17rv5woiV", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("0UiWrxAhlwUJbX01zfsnPc2q5cG3",  "https://www.incognito.fr/actualites/assets-incognito/uploads/2016/05/blog-74-1024x640.jpg", "-L3nJezfRjN17rv5woiV", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("0UiWrxAhlwUJbX01zfsnPc2q5cG3",  "https://www.geo.fr/var/geo/storage/images/voyages/vos-voyages-de-reve/polynesie-escale-a-bora-bora/vaitape-crepuscule/424009-1-fre-FR/coucher-de-soleil-sur-vaitape.jpg", "-L3nJezfRjN17rv5woiV", dated1.getTime());
//
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "https://www.tutsps.com/images/coucher-de-soleil-africain/coucher-de-soleil-africain-32.jpg", "-L3nJezfRjN17rv5woiV", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "http://www.dominiqueduvivier.com/wp-content/uploads/2013/09/Coucher-de-soleil.jpg", "-L3nJezfRjN17rv5woiV", dated1.getTime());
//
//
//        FirebasePhoto.writeNewPhoto("0UiWrxAhlwUJbX01zfsnPc2q5cG3",  "https://c402277.ssl.cf1.rackcdn.com/photos/946/images/story_full_width/forests-why-matter_63516847.jpg?1345534028", "-L3nKVTzIw7rEPeWLQ1S", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("0UiWrxAhlwUJbX01zfsnPc2q5cG3",  "http://www.sgs.com/-/media/global/images/structural-website-images/hero-images/hero-agri-forestry.jpg?la=en&hash=21F8BF2F2E093B090DAB4211AD68B9C7E336BCE1", "-L3nKVTzIw7rEPeWLQ1S", dated1.getTime());
//
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "https://e3.365dm.com/18/01/1096x616/skynews-trees-forest_4200114.jpg?20180107092446", "-L3nKVTzIw7rEPeWLQ1S", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Brazilian_amazon_rainforest.jpg/1200px-Brazilian_amazon_rainforest.jpg", "-L3nKVTzIw7rEPeWLQ1S", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "https://www.muralswallpaper.com/app/uploads/grey-mist-forest-mural-wallpaper-plain.jpg", "-L3nKVTzIw7rEPeWLQ1S", dated1.getTime());

//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "http://www.beaches.com/assets/img/home/rst-btc.jpg", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "http://travel.home.sndimg.com/content/dam/images/travel/fullset/2015/08/03/top-florida-beaches/key-west-beach-florida.jpg.rend.hgtvcom.966.725.suffix/1491580836931.jpeg", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "http://cdn-image.travelandleisure.com/sites/default/files/styles/1600x1000/public/1493321650/barcelona-beach-umbrella-spain-europe-BEACHCITY0417.jpg?itok=xceTXl6U", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("BANe5r3k5SObgcsZxwtJ8ZO15z53",  "https://wa5.jetcdn.com/photos/little-palm-island-resort-spa-a-noble-house-resort/sjeL0BSe/beach-beachfront-coast-hotels.jpeg?x=2005&y=1617&w=992&h=600&dpr=1", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());
//
//
//        FirebasePhoto.writeNewPhoto("0UiWrxAhlwUJbX01zfsnPc2q5cG3",  "https://img1.coastalliving.timeinc.net/sites/default/files/styles/4_3_horizontal_inbody_900x506/public/image/2017/03/main/poipu-beach-park-hawaii_0.jpg?itok=2zTolJLZ", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());


//        FirebasePhoto.writeNewPhoto("3SHHq7w9JdWTrXUirCg7IecBUU12",  "http://www.whatsappdunia.com/uploads/images/5955e93aca0b0.jpg", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());
//        FirebasePhoto.writeNewPhoto("3SHHq7w9JdWTrXUirCg7IecBUU12",  "http://www.twitrcovers.com/wp-content/uploads/2013/02/Beach-Night-l.jpg", "-L3xE6zXNxIW0j04ZUe5", dated1.getTime());

        FirebaseTheme.updatePostAndVoteTheme();
//TODO decommenter
//        FirebaseThemeToVote.updateDateOfThemeToVote(this);

//        MyFirebaseUser.writeNewTestUser("Ludovic Goldak","1","f1");
//        MyFirebaseUser.writeNewTestUser("Anthonin Cocagne","2","f2");
//        MyFirebaseUser.writeNewTestUser("Théophile Blavoet","3","f3");
//        MyFirebaseUser.writeNewTestUser("Quentin Doutriaux","4","f4");
//        MyFirebaseUser.writeNewTestUser("Amine Ben Salah","5","f5");

//        new VoteThemeDialog(this);

//        System.out.println("teston " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
    }

    private void initViews(){
        archivesButton.setBackgroundColor(Color.TRANSPARENT);
        homeButton.setBackgroundColor(Color.TRANSPARENT);
        communityButton.setBackgroundColor(Color.TRANSPARENT);

        archivesButton.setColorFilter(Color.WHITE);
        homeButton.setColorFilter(Color.WHITE);
        communityButton.setColorFilter(Color.WHITE);

        adapter = new MainPageAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(onPageChangeListener);
        pager.setCurrentItem(1);

        //ATTENTION PEUT ETRE DANGEREU
        pager.setOffscreenPageLimit(2);
    }

    @OnClick(R.id.pagerArchivesButton)
    void archivesButtonClickListener(){
        pager.setCurrentItem(0);
    }

    @OnClick(R.id.pagerHomeButton)
    void homebuttonClickListener(){
        pager.setCurrentItem(1);
    }

    @OnClick(R.id.pagerCommunityButton)
    void communityButtonClickListener(){
        pager.setCurrentItem(2);
    }

    ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
//            if(position == 2)
//                ProfilFragment.fragment.getProfilPhotosRecyclerView().updatePhotos();

            changeButtonColor();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    private void changeButtonColor(){
        switch (pager.getCurrentItem()){
            case 0 :
                colorsTools.setCurrentIconTabColorFilter(archivesButton);
                colorsTools.setIconTabColorFilter(homeButton);
                colorsTools.setIconTabColorFilter(communityButton);
                break;
            case 1 :
                colorsTools.setIconTabColorFilter(archivesButton);
                colorsTools.setCurrentIconTabColorFilter(homeButton);
                colorsTools.setIconTabColorFilter(communityButton);
                break;
            case 2 :
                colorsTools.setIconTabColorFilter(archivesButton);
                colorsTools.setIconTabColorFilter(homeButton);
                colorsTools.setCurrentIconTabColorFilter(communityButton);
                break;
        }
    }

    //Appelé dans HomeFragment
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(requestCode == 2) {
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED  ) {
                importPhotoDialog = new ImportPhotoDialog(this);
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("teston ici " + importPhotoDialog.getmCurrentPhotoPath());

        if (requestCode == 1 && resultCode == RESULT_OK) {
            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            //mImageView.setImageBitmap(imageBitmap);
            //System.out.println("teston ici " + importPhotoDialog.getmCurrentPhotoPath());
            galleryAddPic();
            addToFirebase(importPhotoDialog.getmCurrentPhotoPath());
        }

        if (requestCode == 2 && resultCode == RESULT_OK) {
            final Uri imageUri = data.getData();
            addToFirebase(getRealPathFromUri(this, imageUri));

//                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
//                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
//                image_view.setImageBitmap(selectedImage);

        }
    }

    public static String getRealPathFromUri(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    private void addToFirebase(String path){
        MyFirebaseStorage.addToFirebase(this, path, "camera");
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(importPhotoDialog.getmCurrentPhotoPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        if(!FirebaseHandler.isLoggedIn()){
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void onBackPressed() {
        if(pager.getCurrentItem() == 1) {
            if (adapter.getItem(1) instanceof VoteFragment) {
                ((VoteFragment) adapter.getItem(1)).backPressed();
            }
            else if (adapter.getItem(1) instanceof HomeFragment) {
                finish();
            }
        }
    }

    public void setImportPhotoDialog(ImportPhotoDialog importPhotoDialog) {
        this.importPhotoDialog = importPhotoDialog;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        RefWatcher refWatcher = PicWeekApplication.getRefWatcher(MainActivity.this);
//        refWatcher.watch(this);
    }

    public CustomViewPager getPager() {
        return pager;
    }

    public void setPager(CustomViewPager pager) {
        this.pager = pager;
    }
}
