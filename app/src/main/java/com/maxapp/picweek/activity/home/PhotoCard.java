package com.maxapp.picweek.activity.home;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.maxapp.picweek.R;
import com.maxapp.picweek.activity.MainActivity;
import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.maxapp.picweek.model.Photo;
import com.maxapp.picweek.tools.DimensionsTools;
import com.maxapp.picweek.tools.FirebaseHandler;
import com.maxapp.picweek.tools.FrescoImageViewerHandler;
import com.mindorks.placeholderview.SwipePlaceHolderView;
import com.mindorks.placeholderview.annotations.Click;
import com.mindorks.placeholderview.annotations.Layout;
import com.mindorks.placeholderview.annotations.Resolve;
import com.mindorks.placeholderview.annotations.View;
import com.mindorks.placeholderview.annotations.swipe.SwipeCancelState;
import com.mindorks.placeholderview.annotations.swipe.SwipeIn;
import com.mindorks.placeholderview.annotations.swipe.SwipeInState;
import com.mindorks.placeholderview.annotations.swipe.SwipeOut;
import com.mindorks.placeholderview.annotations.swipe.SwipeOutState;
import com.mindorks.placeholderview.annotations.swipe.SwipeTouch;

import java.util.ArrayList;

/**
 * Created by bourdin on 17/12/17
 */

@Layout(R.layout.swipe_card_view)
public class PhotoCard {

    @View(R.id.profileImageView)
    private ImageView photoImageView;

    @View(R.id.mainLayout)
    private FrameLayout mainLayout;

//    @View(R.id.nameAgeTxt)
//    private TextView nameAgeTxt;
//
//    @View(R.id.locationNameTxt)
//    private TextView locationNameTxt;

    private Photo photo;
    private Context mContext;
    private SwipePlaceHolderView mSwipeView;
    private MainActivity mainActivity;

    public PhotoCard(Activity activity, Context context, Photo photo, SwipePlaceHolderView swipeView) {
        mContext = context;
        this.photo = photo;
        mSwipeView = swipeView;
        this.mainActivity = (MainActivity) activity;

//        photoImageView.setOnClickListener(new android.view.View.OnClickListener() {
//            @Override
//            public void onClick(android.view.View view) {
//                imageClick();
//            }
//        });
    }

    @Resolve
    private void onResolved(){
        DimensionsTools dimensionsTools = DimensionsTools.getInstance(mContext);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(dimensionsTools.getScreenX(), dimensionsTools.getPercentOfScreenHeight85());
        mainLayout.setLayoutParams(layoutParams);


        Glide.with(mContext).load(photo.getUrl()).into(photoImageView);
//        nameAgeTxt.setText(mProfile.getName() + ", " + mProfile.getAge());
//        locationNameTxt.setText(mProfile.getLocation());
    }

    @SwipeOut
    private void onSwipedOut(){
        Log.d("EVENT", "onSwipedOut");
        photo.getUsersWhoDislike().add(FirebaseHandler.getFirebaseUser().getUid());
        FirebasePhoto.updatePhotoUsersWhoDislike(photo);
        mainActivity.getPager().setPagingEnabled(true);
    }

    @SwipeCancelState
    private void onSwipeCancelState(){
        Log.d("EVENT", "onSwipeCancelState");
        mainActivity.getPager().setPagingEnabled(true);
    }

    @SwipeIn
    private void onSwipeIn(){
        Log.d("EVENT", "onSwipedIn");
        photo.getUsersWhoLike().add(FirebaseHandler.getFirebaseUser().getUid());
        FirebasePhoto.updatePhotoUsersWhoLike(photo);
        mainActivity.getPager().setPagingEnabled(true);
    }

    @SwipeInState
    private void onSwipeInState(){
        Log.d("EVENT", "onSwipeInState");
        mainActivity.getPager().setPagingEnabled(false);
    }

    @SwipeOutState
    private void onSwipeOutState(){
        Log.d("EVENT", "onSwipeOutState");
        mainActivity.getPager().setPagingEnabled(false);
    }

    @SwipeTouch
    private void onSwipeTouch(){
        Log.d("EVENT", "onSwipeTouch");
        mainActivity.getPager().setPagingEnabled(false);
    }

    @Click(R.id.mainLayout)
    public void imageClick(){
        ArrayList<Photo> photos = new ArrayList<>();
        photos.add(photo);
        mainActivity.getPager().setPagingEnabled(false);
        Log.d("EVENT", "onClick");


        FrescoImageViewerHandler.showVoteImage(mContext, photos );
    }
}
