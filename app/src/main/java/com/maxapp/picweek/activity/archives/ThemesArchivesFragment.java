package com.maxapp.picweek.activity.archives;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.tools.ColorsTools;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThemesArchivesFragment extends Fragment {

    @BindView(R.id.themesArchivesRecyclerView)
    ThemesArchivesRecyclerView themesArchivesRecyclerView;

    @BindView(R.id.themesArchivesSearchView)
    SearchView searchView;

    private ColorsTools colorsTools;

    public ThemesArchivesFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static ThemesArchivesFragment newInstance() {
        ThemesArchivesFragment fragment = new ThemesArchivesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_themes_archives, container, false);
        ButterKnife.bind(this, view);
        colorsTools = new ColorsTools(getContext());
        initViews();
        return view;
    }

    private void initViews(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                themesArchivesRecyclerView.getArchivesRecyclerViewAdapter().getFilter().filter(newText);
                return true;
            }
        });

        ImageView searchIcon = searchView.findViewById(R.id.search_button);
        colorsTools.setSearchIconColorFilter(searchIcon);
    }
}
