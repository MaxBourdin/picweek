package com.maxapp.picweek.activity.profil;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.maxapp.picweek.R;
import com.maxapp.picweek.model.Photo;


/**
 * Created by bourdin on 07/10/17
 */

public class ConsultProfilHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private int position;

    public interface ConsultProfilClickListener {
        void onClick(View view, int position);
    }

    private ImageView photoIV;
    private ConsultProfilClickListener clickListener;

    public ConsultProfilHolder(View itemView, ConsultProfilClickListener clickListener) {
        super(itemView);

        photoIV = itemView.findViewById(R.id.row_profil_photo);

        itemView.setOnClickListener(this);
        this.clickListener = clickListener;
    }

    public void bind(final Photo photo, int position){
        Glide.with(itemView.getContext()).load(photo.getUrl()).into(photoIV);
        this.position = position;
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, this.position);
    }
}
