package com.maxapp.picweek.activity.archives;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebaseTheme;
import com.maxapp.picweek.model.Theme;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;


/**
 * Created by bourdin on 07/10/17
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public interface RecyclerViewClickListener {
        void onClick(View view, String themeId);
    }

    private TextView nameTV, startDateAndNbPhotosTV;
    private RecyclerViewClickListener clickListener;
    private String themeId;
    private ImageView themeImage;
    private Context context;

    //itemView est la vue correspondante à une cellule
    public RecyclerViewHolder(View itemView, RecyclerViewClickListener clickListener) {
        super(itemView);

        this.context = itemView.getContext();
        nameTV = itemView.findViewById(R.id.themeName);
        startDateAndNbPhotosTV = itemView.findViewById(R.id.themeEndDateAndNbPhotos);
        themeImage = itemView.findViewById(R.id.themeImage);

        itemView.setOnClickListener(this);
        this.clickListener = clickListener;
    }

    //Fonction pour remplir la cellule en fonction de l'objet en param
    public void bind(final Theme theme){
        nameTV.setText(theme.getName());
        startDateAndNbPhotosTV.setText(theme.getStartDateString());

        MultiTransformation multi = new MultiTransformation(
                new BlurTransformation(3),
                new ColorFilterTransformation(Color.argb(60, 0, 0, 0)));

        Glide.with(context).load(theme.getUrl())
                .apply(bitmapTransform(multi))
                .into(themeImage);

//        System.out.println("teston la " + theme.getUrl());
//        startDateAndNbPhotosTV.append();

        FirebaseTheme.updateNbPhotosForTheme(theme.getThemeId(), startDateAndNbPhotosTV);

        this.themeId = theme.getThemeId();
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, this.themeId);
    }

}
