package com.maxapp.picweek.activity.community.classement;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxapp.picweek.R;
import com.maxapp.picweek.model.User;


/**
 * Created by bourdin on 07/10/17
 */

public class ClassementRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    public interface RecyclerViewClickListener {
        void onClick(View view, String userId);
    }

    private TextView positionTV, pseudoTV, scoreTV;
    private ImageView avatarIV;
    private RecyclerViewClickListener clickListener;
    private String userId;
    private Context context;

    //itemView est la vue correspondante à une cellule
    public ClassementRecyclerViewHolder(View itemView, RecyclerViewClickListener clickListener) {
        super(itemView);

        this.context = itemView.getContext();
        positionTV = itemView.findViewById(R.id.classementUserPosition);
        pseudoTV = itemView.findViewById(R.id.classementUserPseudo);
        scoreTV = itemView.findViewById(R.id.classementUserScore);
        avatarIV = itemView.findViewById(R.id.classementUserAvatar);

        itemView.setOnClickListener(this);
        this.clickListener = clickListener;
    }

    //Fonction pour remplir la cellule en fonction d'un Tweet
    public void bind(final User user, int position){
        this.userId = user.getId();
        //car il y a le podium avant
        int positionInClassement = position + 4;
        positionTV.setText(Integer.toString(positionInClassement));
        pseudoTV.setText(user.getPseudo());
        scoreTV.setText(Long.toString(user.getNbLikes()));
//        System.out.println("teston " + user.getPhotoURL());
//        Glide.with(itemView.getContext()).load(user.getPhotoURL()).into(avatarIV);

        Glide.with(itemView.getContext()).load(ContextCompat.getDrawable(context, R.drawable.avatar_example)).into(avatarIV);
    }

    @Override
    public void onClick(View view) {
        clickListener.onClick(view, userId);
    }

}
