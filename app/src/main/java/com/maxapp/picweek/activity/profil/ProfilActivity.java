package com.maxapp.picweek.activity.profil;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.maxapp.picweek.firebasemodel.MyFirebaseUser;
import com.maxapp.picweek.tools.ColorsTools;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfilActivity extends AppCompatActivity {

    @BindView(R.id.consultProfilPhotosRecyclerView) ConsultProfilRecyclerView consultProfilRecyclerView;
    @BindView(R.id.consultPseudoTV) TextView pseudoTV;
    @BindView(R.id.consultProfilNbLikesTV) TextView nbLikesTV;
    @BindView(R.id.consultProfilNbPhotosTV) TextView nbPhotosTV;
    @BindView(R.id.consultProfilLikeImage) ImageView likeIV;
    @BindView(R.id.consultProfilCameraImage) ImageView cameraIV;
    @BindView(R.id.consultProfilAvatar) ImageView profilAvatar;
    @BindView(R.id.backProfilButton) ImageButton backProfilButton;

    private String userId;
    private ColorsTools colorsTools;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        ButterKnife.bind(this);

        Intent mIntent = getIntent();
        userId = mIntent.getStringExtra("userId");

        this.colorsTools = new ColorsTools(this);
        initViews();
    }

    private void initViews(){
//        User user = new User(userId, MyFirebaseUser.getName(), MyFirebaseUser.getLargeProfilePhotoURL());
//        UserController userController = new UserController(user, pseudoTV, nbPhotosTV, nbLikesTV, profilAvatar);
//        userController.updateName(user.getPseudo());
//        userController.updateProfilAvatar(user.getPhotoURL(), getContext());
//        FirebasePhoto.updatePhotosAndLikesCountForCurrentUser(userController);
//
//        colorsTools.setCommunityIconColor(likeIV);
//        colorsTools.setCommunityIconColor(cameraIV);
        consultProfilRecyclerView.updatePhotos(userId);
        MyFirebaseUser.updateProfilInformations(userId, pseudoTV, profilAvatar);
        FirebasePhoto.updatePhotosAndLikesCountForUser(userId, nbLikesTV, nbPhotosTV);

        colorsTools.setCommunityIconColor(likeIV);
        colorsTools.setCommunityIconColor(cameraIV);

        colorsTools.setCurrentIconTabColorFilter(backProfilButton);
        ColorsTools.setIconTabBackgroundColor(backProfilButton);
    }

    @OnClick(R.id.backProfilButton)
    void backProfilButtonClickListener(){
//        this.onBackPressed();
        finish();
    }

    public void onBackPressed() {
        finish();
    }
}
