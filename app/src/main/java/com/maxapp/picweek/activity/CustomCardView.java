package com.maxapp.picweek.activity;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by bourdin on 05/02/18
 */

public class CustomCardView  extends CardView{
    int cpt;
    private float xDistance, yDistance, lastX, lastY;
    private Context context;



    public CustomCardView(Context context) {
        super(context);
    }

    public CustomCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        MainActivity.instance.getPager().setPagingEnabled(false);
        Log.d("EVENT", "onTouchEvent");
        return true;
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                xDistance = yDistance = 0f;
//                lastX = event.getX();
//                lastY = event.getY();
//                //return super.onInterceptTouchEvent(event);
//                //return false;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                final float curX = event.getX();
//                final float curY = event.getY();
//                xDistance += Math.abs(curX - lastX);
//                yDistance += Math.abs(curY - lastY);
//                lastX = curX;
//                lastY = curY;
//
//                System.out.println("xDist = " + xDistance + " yDist = " + yDistance);
//
//                if(xDistance > yDistance) {
//                    System.out.println("return true !!!!");
//
//                    MainActivity.instance.getPager().setPagingEnabled(false);
//
////                    MainActivity.instance.onTouchEvent(event);
////                    ((Activity) context).onTouchEvent(event);
//                    //return ((View)this.getParent().getParent()).dispatchTouchEvent(ev);
//                    //return MainActivity.instance.pager.cardViewGroup.dispatchTouchEvent(ev);
//                    //MainActivity.instance.onTouchListener.onTouch(this, ev);
//                    //MainActivity.instance.gestureDetector.onTouchEvent(ev);
//                    return true;
//                }
//                else {
//                    MainActivity.instance.getPager().setPagingEnabled(false);
//                    System.out.println("return false !!!!");
//
//                    System.out.println();
//                    //dispatchTouchEvent(ev);
//                    return false;
//                }
//
//                //break;
//            default:break;
//        }
//        //super.onTouchEvent(ev);
//        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        MainActivity.instance.getPager().setPagingEnabled(false);
        Log.d("EVENT", "onInterceptTouchEvent");

        return true;
//        cpt++;
//        System.out.println("call " + cpt);
//        boolean bool = super.onInterceptTouchEvent(ev);
//        System.out.println("FUCKING CARD onInterceptTouchEvent : " + ev.getAction() + " bool = ");
//
//
//
//
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                xDistance = yDistance = 0f;
//                lastX = ev.getX();
//                lastY = ev.getY();
//                //return super.onInterceptTouchEvent(ev);
//                //return false;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                final float curX = ev.getX();
//                final float curY = ev.getY();
//                xDistance += Math.abs(curX - lastX);
//                yDistance += Math.abs(curY - lastY);
//                lastX = curX;
//                lastY = curY;
//
//                System.out.println("xDist = " + xDistance + " yDist = " + yDistance);
//
//                if(xDistance > yDistance) {
//                    System.out.println("return true !!!!");
//
//                    MainActivity.instance.getPager().setPagingEnabled(false);
//
////                    MainActivity.instance.onTouchEvent(ev);
////                    ((Activity) context).onTouchEvent(ev);
//
//                    //return ((View)this.getParent().getParent()).dispatchTouchEvent(ev);
//                    //return MainActivity.instance.pager.cardViewGroup.dispatchTouchEvent(ev);
//                    //MainActivity.instance.onTouchListener.onTouch(this, ev);
//                    //MainActivity.instance.gestureDetector.onTouchEvent(ev);
//                    return true;
//                }
//                else {
//                    System.out.println("return false !!!!");
//                    MainActivity.instance.getPager().setPagingEnabled(false);
//
//
//                    System.out.println();
//                    //dispatchTouchEvent(ev);
//                    return false;
//                }
//
//                //break;
//            default:break;
//        }
//
//
//
//
//
//        //super.onTouchEvent(ev);
//        return true;
//        //return bool;
//

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        MainActivity.instance.getPager().setPagingEnabled(false);
        Log.d("EVENT", "dispatchTouchEvent");

        return false;
//        switch (ev.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                xDistance = yDistance = 0f;
//                lastX = ev.getX();
//                lastY = ev.getY();
//                //return super.onInterceptTouchEvent(ev);
//                //return false;
//                break;
//            case MotionEvent.ACTION_MOVE:
//                final float curX = ev.getX();
//                final float curY = ev.getY();
//                xDistance += Math.abs(curX - lastX);
//                yDistance += Math.abs(curY - lastY);
//                lastX = curX;
//                lastY = curY;
//
//                System.out.println("xDist = " + xDistance + " yDist = " + yDistance);
//
//                if(xDistance > yDistance) {
//                    System.out.println("return true !!!!");
//
//                    MainActivity.instance.getPager().setPagingEnabled(false);
//
////                    MainActivity.instance.onTouchEvent(ev);
////                    ((Activity) context).onTouchEvent(ev);
//
//                    //return ((View)this.getParent().getParent()).dispatchTouchEvent(ev);
//                    //return MainActivity.instance.pager.cardViewGroup.dispatchTouchEvent(ev);
//                    //MainActivity.instance.onTouchListener.onTouch(this, ev);
//                    //MainActivity.instance.gestureDetector.onTouchEvent(ev);
//
//                    return true;
//                }
//                else {
//                    System.out.println("return false !!!!");
//                    MainActivity.instance.getPager().setPagingEnabled(false);
//
//
//                    System.out.println();
//                    //dispatchTouchEvent(ev);
//                    return super.dispatchTouchEvent(ev);
//                }
//
//                //break;
//            default:break;
//        }
//
//
//
//
//
//        //super.onTouchEvent(ev);
//        return true;
    }
}
