package com.maxapp.picweek.activity.archives;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.maxapp.picweek.R;
import com.maxapp.picweek.model.Theme;

import java.text.Normalizer;
import java.util.ArrayList;

/**
 * Created by bourdin on 07/10/17
 */

public class ArchivesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> implements Filterable {

    private ArrayList<Theme> baseThemeList;
    private ArrayList<Theme> themeFilteredList;
    private RecyclerViewHolder.RecyclerViewClickListener clickListener;

    public ArchivesRecyclerViewAdapter(ArrayList<Theme> baseThemeList) {
        this.baseThemeList = baseThemeList;
        this.themeFilteredList = baseThemeList;
    }

    public void update(ArrayList<Theme> baseThemeList) {
        if(baseThemeList.size() != this.baseThemeList.size()){
            this.baseThemeList = baseThemeList;
            this.themeFilteredList = baseThemeList;
            notifyDataSetChanged();
        }
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_theme,viewGroup,false);
        return new RecyclerViewHolder(view, clickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder myViewHolder, int position) {
        Theme theme = themeFilteredList.get(position);
        myViewHolder.bind(theme);
    }

    @Override
    public int getItemCount() {
        return themeFilteredList.size();
    }

    public void setClickListener(RecyclerViewHolder.RecyclerViewClickListener clickListener){
        this.clickListener = clickListener;
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = getStringWithoutAccent(charSequence.toString());

                if (charString.isEmpty()) {
                    themeFilteredList = baseThemeList;
                } else {
                    ArrayList<Theme> filteredList = new ArrayList<>();
                    for (Theme theme : baseThemeList) {

                        String nameFormat = getStringWithoutAccent(theme.getName());
                        String endDateStringFormat = getStringWithoutAccent(theme.getStartDateString());

                        if (nameFormat.contains(charString) || endDateStringFormat.contains(charString)) {
                            filteredList.add(theme);
                        }
                    }
                    themeFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = themeFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                themeFilteredList = (ArrayList<Theme>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private String getStringWithoutAccent(String string){
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        string = string.replaceAll("[^\\p{ASCII}]", "");
        string = string.replaceAll(" ", "");
        string = string.toLowerCase();
        return string;
    }
}
