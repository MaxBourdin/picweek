package com.maxapp.picweek.activity.home;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.maxapp.picweek.R;

public class CameraActivity extends AppCompatActivity {

    private View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
//        View view  = findViewById(R.id.camera);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mCameraView.start();
    }

    @Override
    protected void onPause() {
//        mCameraView.stop();
        super.onPause();
    }

    public void onBackPressed() {
        finish();
    }
}
