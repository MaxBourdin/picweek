package com.maxapp.picweek.activity.community.classement;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.maxapp.picweek.activity.profil.ProfilActivity;
import com.maxapp.picweek.model.User;

/**
 * Created by bourdin on 20/11/17
 */

public class UserPodium {
    private Context context;
    private User user;
    private RelativeLayout relativeLayout;
    private LinearLayout linearLayout;
    private int position;
    private TextView positionTV, userPseudoTV, scoreTV;
    private ImageView avatarIV;

    public UserPodium(Context context, User user, int position){
        this.context = context;
        this.user = user;
        this.position = position;

        relativeLayout = buildRelativeLayout();
        linearLayout = buildLinearLayout();

        positionTV = buidlTextView(Integer.toString(position),0.1f);
        userPseudoTV = buidlTextView(user.getPseudo(),0.1f);
        scoreTV = buidlTextView(Long.toString(user.getNbLikes()),0.1f);

        positionTV.setTextColor(Color.BLACK);
        userPseudoTV.setTextColor(Color.BLACK);
        scoreTV.setTextColor(Color.BLACK);

        avatarIV = buildImageView(0.7f);

        linearLayout.addView(positionTV);
        linearLayout.addView(avatarIV);
        linearLayout.addView(userPseudoTV);
        linearLayout.addView(scoreTV);

        relativeLayout.addView(linearLayout);
    }

    private LinearLayout buildLinearLayout(){
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

//        if(this.position == 1)
//            layoutParams.weight = 0.4f;
//        else
//            layoutParams.weight = 0.3f;
//
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setGravity(Gravity.CENTER);
        linearLayout.setWeightSum(1.0f);

        return linearLayout;
    }

    private TextView buidlTextView(String value, float weight){
        TextView textView = new TextView(context);
        textView.setText(value);
        textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        TableRow.LayoutParams params = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, weight);
        textView.setLayoutParams(params);
        return textView;
    }

    private ImageView buildImageView(float weight){
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageView.setPadding(5,5,5,5);
        imageView.setAdjustViewBounds(true);
//        imageView.setImageDrawable(user.getDrawable());
        Glide.with(context).load(user.getPhotoURL()).into(imageView);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, weight);
        imageView.setLayoutParams(layoutParams);
        return imageView;
    }

    public void updateTextAndImage(User userTmp, int position){
        positionTV.setText(Integer.toString(position));
        userPseudoTV.setText(userTmp.getPseudo());
        scoreTV.setText(Long.toString(userTmp.getNbLikes()));

//        System.out.println("teston  " + userTmp.getPhotoURL());
        Glide.with(context).load(userTmp.getPhotoURL()).into(avatarIV);
//        avatarIV.setImageDrawable(userTmp.getDrawable());
    }

    private RelativeLayout buildRelativeLayout(){
        RelativeLayout relativeLayout = new RelativeLayout(context);
//        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
//        relativeLayout.setLayoutParams(layoutParams);

        if(this.position == 1)
            relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.4f));
        else
            relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 0.3f));

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ProfilActivity.class);
                intent.putExtra("userId", user.getId());
                context.startActivity(intent);
            }
        });
        return relativeLayout;
    }

    public RelativeLayout getRelativeLayout() {
        return relativeLayout;
    }
}
