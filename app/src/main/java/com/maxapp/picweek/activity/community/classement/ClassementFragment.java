package com.maxapp.picweek.activity.community.classement;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebasePhoto;
import com.maxapp.picweek.model.Classement;
import com.maxapp.picweek.model.PeriodEnum;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassementFragment extends Fragment {

    @BindView(R.id.periodSpinner) Spinner periodSpinner;
    @BindView(R.id.socialSwitchButton) Switch socialSwitchButton;
    @BindView(R.id.podiumParentRL) RelativeLayout parentPodiumRL;
    @BindView(R.id.classementRecyclerView) ClassementRecyclerView classementRecyclerView;

    private Classement classement;

    public ClassementFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static ClassementFragment newInstance() {
        ClassementFragment fragment = new ClassementFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_classement, container, false);
        ButterKnife.bind(this, view);
        initViews();
        return view;
    }

    private void initViews(){
        socialSwitchButton.setOnCheckedChangeListener(onCheckedChangeListener);

        String[] items = new String[]{"Cette semaine", "Ce mois-ci", "Cette année"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, items);
        periodSpinner.setAdapter(adapter);

//        Classement classement = buildClassement();
//        classementRecyclerView.updateAdapter(classement);

//        MyFirebaseUser.updateClassement(getContext(), classementRecyclerView, parentPodiumRL);
        FirebasePhoto.updatePhotoCountForClassement(getContext(), classementRecyclerView, parentPodiumRL);
    }

    private Classement buildClassement(){
        classement = new Classement(getContext(), socialSwitchButton.isChecked(), PeriodEnum.WEEK);
        return classement;
    }

    private void updateClassement(boolean isChecked){
        classement.buildClassement(isChecked);
        classement.getPodium().update(classement.getFirstUser(),classement.getSecondUser(),classement.getThirdUser());
        classementRecyclerView.updateAdapter(classement);
    }

    Switch.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            updateClassement(isChecked);
        }
    };
}
