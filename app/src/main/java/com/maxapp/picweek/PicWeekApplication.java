package com.maxapp.picweek;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by bourdin on 06/01/18
 */

public class PicWeekApplication extends Application {

//    public static RefWatcher getRefWatcher(Context context) {
//        PicWeekApplication application = (PicWeekApplication) context.getApplicationContext();
//        return application.refWatcher;
//    }
//
//    private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();
        //ondestroy de mainActivity a decommenter aussi
//        if (LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }
//        refWatcher = LeakCanary.install(this);


        Fresco.initialize(this);
    }
}
