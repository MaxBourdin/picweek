package com.maxapp.picweek.model;

import android.content.Context;

import com.maxapp.picweek.activity.community.classement.Podium;

import java.util.ArrayList;

/**
 * Created by bourdin on 17/11/17
 */

public class Classement {
    private ArrayList<User> usersAL;
    private boolean social = false;
    private PeriodEnum periodEnum = PeriodEnum.WEEK;
    private User firstUser, secondUser, thirdUser;
    private Context context;
    private Podium podium;

    public Classement(Context context, boolean social, PeriodEnum periodEnum) {
        this.context = context;
        this.social = social;
        this.periodEnum = periodEnum;

//        buildClassement(social);
//        podium = new Podium(context, firstUser, secondUser, thirdUser);
    }

    public Classement(Context context, ArrayList<User> usersAL) {
        this.context = context;
        this.usersAL = usersAL;

        updatePodium();
    }

    public void buildClassement(boolean socialBoolean){
        usersAL = new ArrayList<>();
        if(socialBoolean) {
            User firstUser = new User(context, "Pierre", 25480, 50);
            User secondUser = new User(context, "Paul", 15210, 30);
            User thirdUser = new User(context, "Jacques", 310, 24);

            usersAL.add(firstUser);
            usersAL.add(secondUser);
            usersAL.add(thirdUser);

            for (int i = 0; i < 20; i++) {
                usersAL.add(new User(context, "Bob", 256, 30));
            }
        }else{
            User firstUser = new User(context, "Batman", 25480, 50);
            User secondUser = new User(context, "Superman", 15210, 30);
            User thirdUser = new User(context, "Joker", 310, 24);

            usersAL.add(firstUser);
            usersAL.add(secondUser);
            usersAL.add(thirdUser);

            for(int i = 0 ; i < 20 ; i++){
                usersAL.add(new User(context,"Francis",256,30));
            }
        }

        updatePodium();
    }



    public void updatePodium(){
        firstUser = usersAL.get(0);
        secondUser = usersAL.get(1);
        thirdUser = usersAL.get(2);

        usersAL.remove(2);
        usersAL.remove(1);
        usersAL.remove(0);

        podium = new Podium(context, firstUser, secondUser, thirdUser);

    }

    public Podium getPodium() {
        return podium;
    }

    public User getFirstUser() {
        return firstUser;
    }

    public User getSecondUser() {
        return secondUser;
    }

    public User getThirdUser() {
        return thirdUser;
    }

    public ArrayList<User> getUsersAL() {
        return usersAL;
    }

    public void setUsersAL(ArrayList<User> usersAL) {
        this.usersAL = usersAL;
    }
}
