package com.maxapp.picweek.model;

/**
 * Created by bourdin on 17/11/17
 */

public enum PeriodEnum {
    WEEK,
    MONTH,
    ALL_TIME;
}
