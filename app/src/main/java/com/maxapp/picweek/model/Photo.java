package com.maxapp.picweek.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by bourdin on 17/11/17
 */

public class Photo {
    private String photoId, userId;
    private String url;
    private List<String> usersWhoLike = new ArrayList<>();
    private List<String> usersWhoDislike = new ArrayList<>();
    private Date date;
    private int nbVotes;

    public Photo(String photoId, String userId, String photoUrl, List<String> usersWhoLike, List<String> usersWhoDislike, long date){
        this.photoId = photoId;
        this.userId = userId;
        this.url = photoUrl;
        this.usersWhoLike = usersWhoLike;
        this.usersWhoDislike = usersWhoDislike;
        this.date = new Date(date);
    }

    //Compte le nombre de fois on la photo a été vue
    //pour afficher équitablement les photos aux différents users
    public void calculateNbVotes(){
        this.nbVotes = usersWhoLike.size() + usersWhoDislike.size();
    }

    //Verifie si l'utilisateur a déjà voté pour cette photo
    public boolean userNeverVote(String userWhoVoteId){
        if(!usersWhoLike.contains(userWhoVoteId) && !usersWhoDislike.contains(userWhoVoteId))
            return true;
        else
            return false;
    }

    public int getNbLikes(){
        return getUsersWhoLike().size();
    }

    public List<String> getUsersWhoLike() {
        return usersWhoLike;
    }

    public List<String> getUsersWhoDislike() {
        return usersWhoDislike;
    }

    public String getUrl() {
        return url;
    }

    public String getPhotoId() {
        return photoId;
    }

    public void setPhotoId(String photoId) {
        this.photoId = photoId;
    }

    public String getUserId() {
        return userId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getNbVotes() {
        return nbVotes;
    }
}
