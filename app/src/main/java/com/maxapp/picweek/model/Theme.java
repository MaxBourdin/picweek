package com.maxapp.picweek.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bourdin on 17/11/17
 */

public class Theme {
    private String name, startDateString, themeId, url;
    private Date startDate;

    public Theme(String themeId, String name, String url, long startDate) {
        this.themeId = themeId;
        this.name = name;
        this.url = url;
        this.startDate = new Date(startDate);

        SimpleDateFormat formater = new SimpleDateFormat("dd MMMM yyyy");
        startDateString = formater.format(this.startDate);
    }

    public Theme() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartDateString() {
        return startDateString;
    }

    public String getThemeId() {
        return themeId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public String getUrl() {
        return url;
    }
}
