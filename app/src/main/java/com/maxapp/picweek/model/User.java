package com.maxapp.picweek.model;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.maxapp.picweek.R;

import java.util.ArrayList;

/**
 * Created by bourdin on 17/11/17
 */

public class User {
    private String id;
    private String pseudo;
    private String photoURL;
    private long nbPhotos;
    private long nbLikes;
    private ArrayList<Photo> userPhotosAL;
    private Drawable drawable;

    //A supprimer (ou pas)
    public User(Context context, String pseudo, long nbLikes, long nbPhotos) {
        this.pseudo = pseudo;
        this.nbLikes = nbLikes;
        this.nbPhotos = nbPhotos;
        drawable = context.getResources().getDrawable(R.drawable.avatar_example);
    }

    public User(String id, String pseudo, String photoURL) {
        this.id = id;
        this.pseudo = pseudo;
        this.photoURL = photoURL;
    }

    public User(String id, String pseudo, String photoURL, long nbLikes) {
        this.id = id;
        this.pseudo = pseudo;
        this.photoURL = photoURL;
        this.nbLikes = nbLikes;
    }

    public String getPseudo() {
        return pseudo;
    }

    public long getNbLikes() {
        return nbLikes;
    }

    public long getNbPhotos() {
        return nbPhotos;
    }

    public Drawable getDrawable() {
        return drawable;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setNbLikes(long nbLikes) {
        this.nbLikes = nbLikes;
    }

    public void setNbPhotos(long nbPhotos) {
        this.nbPhotos = nbPhotos;
    }

    public ArrayList<Photo> getUserPhotosAL() {
        return userPhotosAL;
    }

    public void setUserPhotosAL(ArrayList<Photo> userPhotosAL) {
        this.userPhotosAL = userPhotosAL;
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
    }
}
