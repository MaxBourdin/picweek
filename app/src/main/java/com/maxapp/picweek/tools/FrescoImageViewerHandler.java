package com.maxapp.picweek.tools;

import android.content.Context;

import com.maxapp.picweek.activity.archives.theme.OverlayView;
import com.maxapp.picweek.firebasemodel.MyFirebaseUser;
import com.maxapp.picweek.model.Photo;
import com.stfalcon.frescoimageviewer.ImageViewer;

import java.util.ArrayList;

/**
 * Created by bourdin on 08/01/18
 */

public class FrescoImageViewerHandler {
    public static void showArchivesImages(Context context, final ArrayList<Photo> photos, int startPosition){
        final OverlayView overlayView = new OverlayView(context);

        new ImageViewer.Builder<>(context, photos)
                .setFormatter(new ImageViewer.Formatter<Photo>() {
                    @Override
                    public String format(Photo photo) {
                        return photo.getUrl();
                    }
                })
                .setStartPosition(startPosition)
//                .hideStatusBar(false)
                .setImageChangeListener(new ImageViewer.OnImageChangeListener() {
                    @Override
                    public void onImageChange(int position) {
                        Photo photo = photos.get(position);
                        MyFirebaseUser.updateOverlayNameForUser(photo.getUserId(), overlayView);
//                        overlayView.setUserPseudo(photo.getUserPseudo());
                        overlayView.setNbLikes(photo.getUsersWhoLike().size());
                    }
                })
                .setOverlayView(overlayView)
                .show();
    }

    public static void showCurrentProfilImages(Context context, final ArrayList<Photo> photos, int startPosition){
        final OverlayView overlayView = new OverlayView(context);

        new ImageViewer.Builder<>(context, photos)
                .setFormatter(new ImageViewer.Formatter<Photo>() {
                    @Override
                    public String format(Photo photo) {
                        return photo.getUrl();
                    }
                })
                .setStartPosition(startPosition)
//                .hideStatusBar(false)
                .setImageChangeListener(new ImageViewer.OnImageChangeListener() {
                    @Override
                    public void onImageChange(int position) {
                        Photo photo = photos.get(position);
                        overlayView.setUserPseudo("");
                        overlayView.setNbLikes(photo.getUsersWhoLike().size());
                    }
                })
                .setOverlayView(overlayView)
                .show();
    }

    public static void showVoteImage(Context context, final ArrayList<Photo> photos){
        new ImageViewer.Builder<>(context, photos)
                .setFormatter(new ImageViewer.Formatter<Photo>() {
                    @Override
                    public String format(Photo photo) {
                        return photo.getUrl();
                    }
                })
                .setStartPosition(0)
                .show();
    }

}
