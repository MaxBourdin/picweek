package com.maxapp.picweek.tools;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by bourdin on 24/01/18
 */

public class FirebaseHandler {
    public static boolean isLoggedIn() {
        return getFirebaseUser() != null;
    }

    public static FirebaseUser getFirebaseUser(){
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    public static FirebaseDatabase getFirebaseDatabase(){
        return FirebaseDatabase.getInstance();
    }

    public static void firebaseSignOut(){
        FirebaseAuth.getInstance().signOut();
    }
}
