package com.maxapp.picweek.tools;

import com.maxapp.picweek.model.Photo;

import java.util.Comparator;

/**
 * Created by bourdin on 28/01/18
 * Classe permettant de trier les photos par nombre de votes
 */
public class PhotoNbVotesComparator implements Comparator<Photo> {
    @Override
    public int compare(Photo photo1, Photo photo2) {
        return photo1.getNbVotes() - photo2.getNbVotes();
//        return photo1.getNbLikes().compareTo(photo1.getNbLikes());
    }
}
