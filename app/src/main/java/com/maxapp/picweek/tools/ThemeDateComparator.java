package com.maxapp.picweek.tools;

import com.maxapp.picweek.model.Theme;

import java.util.Comparator;

/**
 * Created by bourdin on 28/01/18
 * Classe permettant de trier les themes par dates
 */
public class ThemeDateComparator implements Comparator<Theme> {
    @Override
    public int compare(Theme theme1, Theme theme2) {
        return theme2.getStartDate().compareTo(theme1.getStartDate());
    }
}
