package com.maxapp.picweek.tools;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by bourdin on 06/02/18
 */

public class SharedPreferenceHandler {

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SharedPreferenceHandler(Context context){
        sharedPreferences = context.getSharedPreferences("picweek", Context.MODE_PRIVATE);

    }

    public void setDateThemeToVote(long date){
        editor = sharedPreferences.edit();
        editor.putLong("voteThemeDate" , date);
        editor.commit();
    }

    public long getDateThemeToVote(){
        return sharedPreferences.getLong("voteThemeDate" , 0);
    }

    /** Instance unique non préinitialisée */
    private static SharedPreferenceHandler INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static synchronized SharedPreferenceHandler getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new SharedPreferenceHandler(context);
        }

        return INSTANCE;
    }
}
