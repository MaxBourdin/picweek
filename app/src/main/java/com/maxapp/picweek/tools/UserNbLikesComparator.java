package com.maxapp.picweek.tools;

import com.maxapp.picweek.model.User;

import java.util.Comparator;

/**
 * Created by bourdin on 28/01/18
 * Classe permettant de trier les users par nombre de likes (décroissant) dans le classement
 */
public class UserNbLikesComparator implements Comparator<User> {
    @Override
    public int compare(User user1, User user2) {
        return (int) (user2.getNbLikes() - user1.getNbLikes());
//        return photo1.getNbLikes().compareTo(photo1.getNbLikes());
    }
}
