package com.maxapp.picweek.tools;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;

/**
 * Created by bourdin on 17/11/17
 */

public class DimensionsTools {
    private int screenX, screenY;
    private Context context;
    private int percentOfScreenHeight85;

    private DimensionsTools(Context context) {
        this.context = context;
        Activity activity = (Activity) context;

        //Getting display object
        Display display = activity.getWindowManager().getDefaultDisplay();

        //Getting the screen resolution into point object
        Point size = new Point();
        display.getSize(size);

        screenX = size.x;
        screenY = size.y;

        percentOfScreenHeight85 = getPercentOf(screenY, 85);
    }

    /** Instance unique non préinitialisée */
    private static DimensionsTools INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static synchronized DimensionsTools getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DimensionsTools(context);
        }
        return INSTANCE;
    }

    public void setDialogDimensions(Dialog dialog){
        int width = getPercentOf(screenX, 85);
        int height = getPercentOf(screenY, 25);
        dialog.getWindow().setLayout(width,height);
    }

    public int getWidthSizeByPercent(int percent){
        double value = 100.0 / percent; // Ex 100 / 90 = 1.1;
        return (int) (screenX / value); // et donc ici screenX / 1.1 ( 90% de l'écran )
    }

    public int getHeightSizeByPercent(int percent){
        double value = 100.0 / percent; // Ex 100 / 90 = 1.1;
        return (int) (screenY / value); // et donc ici screenX / 1.1 ( 90% de l'écran )
    }

    public int getPercentOf(int value, int percentage){
        int k = (int)(value*(percentage/100.0f));
        return k;
    }

    public int getScreenX() {
        return screenX;
    }

    public int getScreenY() {
        return screenY;
    }

    public int getPercentOfScreenHeight85() {
        return percentOfScreenHeight85;
    }
}
