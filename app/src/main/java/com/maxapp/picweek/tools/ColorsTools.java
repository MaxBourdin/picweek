package com.maxapp.picweek.tools;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.maxapp.picweek.R;

/**
 * Created by bourdin on 20/11/17
 */
@SuppressLint("ResourceType")
public class ColorsTools {

    private Context context;
    public ColorsTools(Context context){
        this.context = context;
    }

    public void setCurrentIconTabColorFilter(ImageButton imageButton){
        imageButton.setColorFilter(Color.parseColor(context.getResources().getString(R.color.tabIconSelectedColorFilter)));
    }

    public void setCommunityIconColor(ImageView imageView){
        imageView.setColorFilter(Color.parseColor(context.getResources().getString(R.color.communityIconColor)));
    }

    public void setIconTabColorFilter(ImageButton imageButton){
        imageButton.setColorFilter(Color.parseColor(context.getResources().getString(R.color.tabIconsColorFilter)));
    }

    public void setSearchIconColorFilter(ImageView imageView){
        imageView.setColorFilter(Color.parseColor(context.getResources().getString(R.color.searchIconColorFilter)));
    }
//    public static void setCurrentCommunityButtonTabBackgroundColor(Button button){
//        button.setBackgroundColor(Color.GRAY);
//    }

//    public static void setCommunityButtonTabBackgroundColor(Button button){
//        button.setBackgroundColor(Color.BLACK);
//    }

    public void setCurrentCommunityButtonTabBackgroundColor(Button button){
        button.setBackgroundColor(Color.parseColor(context.getResources().getString(R.color.communityTabSelectedBackgroundColor)));
    }

    public void setCommunityButtonTabBackgroundColor(Button button){
        button.setBackgroundColor(Color.parseColor(context.getResources().getString(R.color.communityTabBackgroundColor)));
    }

    public static void setIconTabBackgroundColor(ImageButton imageButton){
        imageButton.setBackgroundColor(Color.TRANSPARENT);
    }
}
