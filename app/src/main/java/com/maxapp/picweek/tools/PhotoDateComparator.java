package com.maxapp.picweek.tools;

import com.maxapp.picweek.model.Photo;

import java.util.Comparator;

/**
 * Created by bourdin on 28/01/18
 * Classe permettant de trier les photos par date
 */
public class PhotoDateComparator implements Comparator<Photo> {
    @Override
    public int compare(Photo photo1, Photo photo2) {
        return photo2.getDate().compareTo(photo1.getDate());
//        return photo1.getNbLikes().compareTo(photo1.getNbLikes());
    }
}
