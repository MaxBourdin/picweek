package com.maxapp.picweek.tools;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.maxapp.picweek.activity.login.LoginActivity;

import java.util.Arrays;
import java.util.List;

/**
 * Created by bourdin on 24/01/18
 */

public class FacebookHandler {
    private static FirebaseAuth mAuth;
    private static final int RC_SIGN_IN = 123;

    public static void facebookConnection(LoginActivity loginActivity) {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
//                new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build(),
//                new AuthUI.IdpConfig.Builder(AuthUI.PHONE_VERIFICATION_PROVIDER).build(),
//                new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build(),
                new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build());

        // Create and launch sign-in intent
        loginActivity.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }
}
