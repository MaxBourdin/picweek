package com.maxapp.picweek.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.maxapp.picweek.R;
import com.maxapp.picweek.firebasemodel.FirebaseThemeToVote;
import com.maxapp.picweek.tools.DimensionsTools;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bourdin on 17/11/17
 */

public class VoteThemeDialog extends Dialog {
    @BindView(R.id.theme1Image)ImageView theme1Image;
    @BindView(R.id.theme2Image)ImageView theme2Image;
    @BindView(R.id.theme3Image)ImageView theme3Image;
    @BindView(R.id.theme4Image)ImageView theme4Image;

    @BindView(R.id.theme1Text)TextView theme1Text;
    @BindView(R.id.theme2Text)TextView theme2Text;
    @BindView(R.id.theme3Text)TextView theme3Text;
    @BindView(R.id.theme4Text)TextView theme4Text;

    private Context context;

    public VoteThemeDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_theme_vote);
        ButterKnife.bind(this);

        int width = DimensionsTools.getInstance(context).getWidthSizeByPercent(100); //un dialog dispose d'un padding par défault je suppose donc 100% <=> 90% environ
        int height = DimensionsTools.getInstance(context).getHeightSizeByPercent(75);
        getWindow().setLayout(width, height);
        getWindow().getAttributes().windowAnimations = R.style.DialogUpToUp;

//        MultiTransformation multi = new MultiTransformation(
//                new BlurTransformation(3),
//                new ColorFilterTransformation(Color.argb(60, 0, 0, 0)));
//
//        Glide.with(context).load("https://www.lesechos.fr/medias/2017/10/12/2121772_la-foret-un-placement-securisant-dans-le-temps-web-tete-030682267356.jpg")
//                .apply(bitmapTransform(multi))
//                .into(theme1Image);
//
//        theme1Text.setText("Teston lala");


        FirebaseThemeToVote.updateThemesToVote(getContext(), theme1Image, theme2Image, theme3Image, theme4Image, theme1Text, theme2Text, theme3Text, theme4Text);

        show();
    }

    @OnClick(R.id.voteLater)
    void voteLater(){
        dismiss();
    }

    @OnClick(R.id.theme1Layout)
    void theme1LayoutClick(){
        FirebaseThemeToVote.incrementNbVotesForTheme(context, "theme1");
        dismiss();
    }

    @OnClick(R.id.theme2Layout)
    void theme2LayoutClick(){
        FirebaseThemeToVote.incrementNbVotesForTheme(context, "theme2");
        dismiss();
    }

    @OnClick(R.id.theme3Layout)
    void theme3LayoutClick(){
        FirebaseThemeToVote.incrementNbVotesForTheme(context, "theme3");
        dismiss();
    }

    @OnClick(R.id.theme4Layout)
    void theme4LayoutClick(){
        FirebaseThemeToVote.incrementNbVotesForTheme(context, "theme4");
        dismiss();
    }
}
