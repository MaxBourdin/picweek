package com.maxapp.picweek.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.view.Gravity;
import android.view.Window;
import android.widget.Button;

import com.maxapp.picweek.R;
import com.maxapp.picweek.tools.DimensionsTools;
import com.maxapp.picweek.tools.FirebaseHandler;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bourdin on 31/01/18
 */

public class ImportPhotoDialog extends Dialog {

    @BindView(R.id.importPhotoButton)public Button importPhotoButton;
    @BindView(R.id.takePhotoButton)public Button takePhotoButton;

//    final static int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_IMPORT_PHOTO = 2;
    private Context context;
    String mCurrentPhotoPath;


    public ImportPhotoDialog(@NonNull Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_import_photo);
        this.context = context;
        ButterKnife.bind(this);
        DimensionsTools.getInstance(context).setDialogDimensions(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().getAttributes().windowAnimations = R.style.DialogDownToDown;
        getWindow().getAttributes().gravity = Gravity.BOTTOM;

        show();
    }

    @OnClick(R.id.importPhotoButton)
    public void importPhotoButtonClick(){
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        ((Activity) context).startActivityForResult(photoPickerIntent, REQUEST_IMPORT_PHOTO);
        dismiss();
    }

    @OnClick(R.id.takePhotoButton)
    public void takePhotoButtonClick(){
        dispatchTakePictureIntent();
        dismiss();
    }

    private void dispatchTakePictureIntent() {
//        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
//            ((Activity) context).startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
//        }

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                System.out.println("teston2 " + photoFile.getAbsolutePath());
                System.out.println("teston3 " + context.getPackageName() + ".fileprovider");
                Uri photoURI = FileProvider.getUriForFile(context,
                        context.getPackageName() + ".fileprovider",
                        photoFile);

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                ((Activity) context).startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "PICWEEK_JPEG_" + timeStamp + "_" + FirebaseHandler.getFirebaseUser().getUid() + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

//        /storage/emulated/0/Pictures/JPEG_20180131_153016_1363280206.jpg

        System.out.println("teston1 " + image.getAbsolutePath());

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public String getmCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");
//            mImageView.setImageBitmap(imageBitmap);
//        }
//    }


//    public PauseDialog(@NonNull Context context) {
//        super(context);
//
//
//        Tools tools = new Tools(context);
//        getWindow().setLayout(tools.getDialogWidth(), tools.getDialogHeight());
//
//        initViews();
//        initTextSize(tools);
//        initButtonColor(tools);
//        initPieces(tools);
//
//        this.sharedPreferencesHandler = new SharedPreferencesHandler(context);
//        checkSound();
//        show();
//    }
}
