package com.maxapp.picweek.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.Window;

import com.maxapp.picweek.R;
import com.maxapp.picweek.tools.DimensionsTools;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by bourdin on 31/01/18
 */

public class UnSuccessUploadDialog extends Dialog {

    public UnSuccessUploadDialog(@NonNull Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_unsuccess_upload);
        ButterKnife.bind(this);

        DimensionsTools.getInstance(context).setDialogDimensions(this);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        getWindow().getAttributes().windowAnimations = R.style.DialogDownToDown;
//        getWindow().getAttributes().gravity = Gravity.BOTTOM;

        show();
    }

    @OnClick(R.id.dialogUnSuccessRoot)
    public void click(){
        dismiss();
    }
}
