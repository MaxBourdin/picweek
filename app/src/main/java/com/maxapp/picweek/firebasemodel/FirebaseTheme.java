package com.maxapp.picweek.firebasemodel;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maxapp.picweek.activity.archives.ArchivesRecyclerViewAdapter;
import com.maxapp.picweek.model.Theme;
import com.maxapp.picweek.tools.FirebaseHandler;
import com.maxapp.picweek.tools.ThemeDateComparator;

import java.util.ArrayList;
import java.util.Collections;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
import static com.maxapp.picweek.firebasemodel.FirebasePhoto.PHOTOS_COLUMN;
import static com.maxapp.picweek.firebasemodel.FirebasePhoto.THEMEID_COLUMN;

/**
 * Created by bourdin on 26/01/18
 */

public class FirebaseTheme {
    public final static String THEMES_COLUMN = "themes";
    public final static String DATE_COLUMN = "startDate";

    public String name, url;
    public long startDate;

    public FirebaseTheme(){

    }

    public FirebaseTheme(String name, String url, long startDate) {
        this.name = name;
        this.url = url;
        this.startDate = startDate;
    }

    //Ajoute une photo en BDD
    public static void writeNewTheme(String name, String url, long startDate) {
        FirebaseTheme theme = new FirebaseTheme(name, url, startDate);
        FirebaseHandler.getFirebaseDatabase().getReference(THEMES_COLUMN).push().setValue(theme);
    }

    //Update la liste des themes
    public static void updateThemesList(final ArchivesRecyclerViewAdapter archivesRecyclerViewAdapter) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(THEMES_COLUMN);
//        Query query = myRef.orderByChild(DATE_COLUMN).equalTo(FirebaseCurrentUserSingleton.getInstance().getUsrId());
        Query query = FirebaseHandler.getFirebaseDatabase().getReference(THEMES_COLUMN).orderByChild(DATE_COLUMN);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot themesSnapshop) {
                if(themesSnapshop.exists()){
                    ArrayList<Theme> themes = new ArrayList<>();

                    for (DataSnapshot themeSnapshot : themesSnapshop.getChildren()) {
                        FirebaseTheme firebaseTheme = themeSnapshot.getValue(FirebaseTheme.class);
                        Theme theme = new Theme(themeSnapshot.getKey(), firebaseTheme.name, firebaseTheme.url, firebaseTheme.startDate);
                        themes.add(theme);
                    }

                    Collections.reverse(themes);
                    Collections.sort(themes, new ThemeDateComparator());

                    //On supprime les thèmes pour poster et voter
                    if(themes.size() > 1) {
                        themes.remove(0); // Theme pour Poster
                        themes.remove(0); // Theme pour voter
                    }else if (themes.size() > 0){
                        themes.remove(0); // Theme pour poster
                    }

                    archivesRecyclerViewAdapter.update(themes);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Update le thème pour poster et pour voter
    public static void updatePostAndVoteTheme() {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(THEMES_COLUMN);
        Query query = myRef.orderByChild(DATE_COLUMN);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot themesSnapshop) {
                if(themesSnapshop.exists()){
                    ArrayList<Theme> themes = new ArrayList<>();

                    for (DataSnapshot themeSnapshot : themesSnapshop.getChildren()) {
                        FirebaseTheme firebaseTheme = themeSnapshot.getValue(FirebaseTheme.class);
                        Theme theme = new Theme(themeSnapshot.getKey(), firebaseTheme.name, firebaseTheme.url, firebaseTheme.startDate);
                        themes.add(theme);
                    }

                    Collections.sort(themes, new ThemeDateComparator());

                    CurrentPostAndVoteTheme.getInstance().setPostTheme(themes.get(0));
                    CurrentPostAndVoteTheme.getInstance().setVoteTheme(themes.get(1));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void updatePostAndVoteThemeFromHomeFragment(final Context context, final ImageView voteIV, final ImageView postIV, final TextView voteTV, final TextView postTV) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(THEMES_COLUMN);
        Query query = myRef.orderByChild(DATE_COLUMN);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot themesSnapshop) {
                if(themesSnapshop.exists()){

                    Theme voteTheme = CurrentPostAndVoteTheme.getInstance().getVoteTheme();
                    Theme postTheme = CurrentPostAndVoteTheme.getInstance().getPostTheme();

                    MultiTransformation multi = new MultiTransformation(
                            new BlurTransformation(3),
                            new ColorFilterTransformation(Color.argb(60, 0, 0, 0)));

                    Glide.with(context).load(voteTheme.getUrl())
                            .apply(bitmapTransform(multi))
                            .into(voteIV);

                    Glide.with(context).load(postTheme.getUrl())
                            .apply(bitmapTransform(multi))
                            .into(postIV);

                    voteTV.setText(voteTheme.getName());
                    postTV.setText(postTheme.getName());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Update le nombre de photos pour le theme passé en param
    public static void updateNbPhotosForTheme(String themeId, final TextView textView) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        Query query = myRef.orderByChild(THEMEID_COLUMN).equalTo(themeId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot themesSnapshop) {
                if(themesSnapshop.exists()){
                    long nbPhotos = themesSnapshop.getChildrenCount();
                    String text = (String) textView.getText();
                    textView.setText(text + " - " + Long.toString(nbPhotos) + " photos");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Update le nom du theme
    public static void updateThemeName(String themeId, final TextView textView) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(THEMES_COLUMN);
        Query query = myRef.orderByKey().equalTo(themeId);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot themesSnapshot) {
                if(themesSnapshot.exists()){
                    for (DataSnapshot themeSnapshot : themesSnapshot.getChildren()) {
                        FirebaseTheme firebaseTheme = themeSnapshot.getValue(FirebaseTheme.class);
                        textView.setText(firebaseTheme.name);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
