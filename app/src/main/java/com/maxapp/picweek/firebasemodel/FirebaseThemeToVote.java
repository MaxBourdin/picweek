package com.maxapp.picweek.firebasemodel;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maxapp.picweek.dialog.VoteThemeDialog;
import com.maxapp.picweek.tools.FirebaseHandler;
import com.maxapp.picweek.tools.SharedPreferenceHandler;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;

/**
 * Created by bourdin on 26/01/18
 */

public class FirebaseThemeToVote {
    public final static String THEMESTOVOTE_COLUMN = "themesToVote";
    public final static String THEMESTOVOTETHEMES_COLUMN = "themes";

    public String name, url;
    public long nbVotes;

    public FirebaseThemeToVote(){

    }

    public FirebaseThemeToVote(String name, String url, long nbVotes) {
        this.name = name;
        this.url = url;
        this.nbVotes = nbVotes;
    }

    //Update le thème pour poster et pour voter
    public static void updateThemesToVote(
            final Context context,
            final ImageView theme1Image, final ImageView theme2Image, final ImageView theme3Image, final ImageView theme4Image,
            final TextView theme1Text, final TextView theme2Text, final TextView theme3Text, final TextView theme4Text) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(THEMESTOVOTE_COLUMN);
        Query query = myRef.orderByKey().getRef().orderByChild(THEMESTOVOTETHEMES_COLUMN);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot themesSnapshot) {
                if(themesSnapshot.exists()) {
                    FirebaseThemeToVote firebaseThemeToVote1 = null;
                    FirebaseThemeToVote firebaseThemeToVote2 = null;
                    FirebaseThemeToVote firebaseThemeToVote3 = null;
                    FirebaseThemeToVote firebaseThemeToVote4 = null;
//                    System.out.println("teston childcount " + themesSnapshot.getChildrenCount());
                    for (DataSnapshot themeSnaphot : themesSnapshot.getChildren()) {
                        firebaseThemeToVote1 = themeSnaphot.child("themes").child("theme1").getValue(FirebaseThemeToVote.class);
                        firebaseThemeToVote2 = themeSnaphot.child("themes").child("theme2").getValue(FirebaseThemeToVote.class);
                        firebaseThemeToVote3 = themeSnaphot.child("themes").child("theme3").getValue(FirebaseThemeToVote.class);
                        firebaseThemeToVote4 = themeSnaphot.child("themes").child("theme4").getValue(FirebaseThemeToVote.class);
                    }

                    MultiTransformation multi = new MultiTransformation(
                            new BlurTransformation(3),
                            new ColorFilterTransformation(Color.argb(60, 0, 0, 0)));
		    if( firebaseThemeToVote1 != null )
                    	updateTextAndImage(context, firebaseThemeToVote1, theme1Image, theme1Text, multi);
		    if( firebaseThemeToVote2 != null )
                    updateTextAndImage(context, firebaseThemeToVote2, theme2Image, theme2Text, multi);
		    if( firebaseThemeToVote3 != null )
                    updateTextAndImage(context, firebaseThemeToVote3, theme3Image, theme3Text, multi);
		    if( firebaseThemeToVote4 != null )
                    updateTextAndImage(context, firebaseThemeToVote4, theme4Image, theme4Text, multi);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void incrementNbVotesForTheme(final Context context, final String name) {
        Query query = FirebaseHandler.getFirebaseDatabase().getReference(THEMESTOVOTE_COLUMN).orderByKey().limitToLast(1);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    FirebaseThemeToVote firebaseThemeToVote = childSnapshot.child("themes").child(name).getValue(FirebaseThemeToVote.class);
                    long value = firebaseThemeToVote.nbVotes +1;
                    childSnapshot.getRef().child("themes").child(name).child("nbVotes").setValue(value);

                    long date = (long) childSnapshot.child("startVoteDate").getValue();
                    SharedPreferenceHandler.getInstance(context).setDateThemeToVote(date);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void updateDateOfThemeToVote(final Context context) {
        Query query = FirebaseHandler.getFirebaseDatabase().getReference(THEMESTOVOTE_COLUMN).orderByKey().limitToLast(1);

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    long date = (long) childSnapshot.child("startVoteDate").getValue();
                    if(SharedPreferenceHandler.getInstance(context).getDateThemeToVote() != date){
                        new VoteThemeDialog(context);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private static void updateTextAndImage(Context context, FirebaseThemeToVote firebaseThemeToVote, ImageView imageView, TextView textView, MultiTransformation multiTransformation){
        Glide.with(context).load(firebaseThemeToVote.url)
                .apply(bitmapTransform(multiTransformation))
                .into(imageView);

        textView.setText(firebaseThemeToVote.name);
    }
}
