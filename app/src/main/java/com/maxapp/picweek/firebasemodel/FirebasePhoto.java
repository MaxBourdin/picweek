package com.maxapp.picweek.firebasemodel;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maxapp.picweek.activity.archives.theme.ThemeRecyclerView;
import com.maxapp.picweek.activity.community.classement.ClassementRecyclerView;
import com.maxapp.picweek.activity.community.currentuserprofil.ProfilPhotosRecyclerView;
import com.maxapp.picweek.activity.home.PhotoCard;
import com.maxapp.picweek.activity.profil.ConsultProfilRecyclerView;
import com.maxapp.picweek.model.Photo;
import com.maxapp.picweek.tools.FirebaseHandler;
import com.maxapp.picweek.tools.PhotoDateComparator;
import com.maxapp.picweek.tools.PhotoNbLikesComparator;
import com.mindorks.placeholderview.SwipePlaceHolderView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by bourdin on 24/01/18
 */

public class FirebasePhoto {
    public final static String PHOTOS_COLUMN = "photos";
    public final static String USERID_COLUMN = "userId";
    public final static String USERSWHOLIKE_COLUMN = "usersWhoLike";
    public final static String USERSWHODISLIKE_COLUMN = "usersWhoDislike";
    public final static String PHOTOURL_COLUMN = "url";
    public final static String THEMEID_COLUMN = "themeId";
    public final static String DATE_COLUMN = "date";

    public String userId, url, themeId;
    public long date;
    public List<String> usersWhoLike = new ArrayList<>();
    public List<String> usersWhoDislike = new ArrayList<>();

    public FirebasePhoto() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public FirebasePhoto(String userId, String url, String themeId, long date) {
        this.userId = userId;
        this.url = url;
        this.themeId = themeId;
        this.date = date;

//        usersWhoLike.add("1");
//        usersWhoDislike.add("2");
    }

    //Ajoute une photo en BDD
    public static void writeNewPhoto(String userId, String url, String themeId, long date) {
        FirebasePhoto photo = new FirebasePhoto(userId, url, themeId, date);
        FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN).push().setValue(photo);
    }

    public static void writeNewPhoto(String url) {
        String userId = FirebaseHandler.getFirebaseUser().getUid();
        String themeId = CurrentPostAndVoteTheme.getInstance().getPostTheme().getThemeId();
        long date = new Date().getTime();

        FirebasePhoto photo = new FirebasePhoto(userId, url, themeId, date);
        FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN).push().setValue(photo);
    }

    public static void updatePhotoUsersWhoDislike(final Photo photo) {
        DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        myRef.child(photo.getPhotoId()).child(USERSWHODISLIKE_COLUMN).setValue(photo.getUsersWhoDislike());
    }

    public static void updatePhotoUsersWhoLike(final Photo photo) {
        DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        myRef.child(photo.getPhotoId()).child(USERSWHOLIKE_COLUMN).setValue(photo.getUsersWhoLike());
    }

    public static void updateThemePhotos(String themeId, final ThemeRecyclerView themeRecyclerView) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        Query query = myRef.orderByChild(THEMEID_COLUMN).equalTo(themeId);

        //Before  addValueEventListener
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot photosSnapshot) {
                if(photosSnapshot.exists()){
                    ArrayList<Photo> photos = new ArrayList<>();

                    for (DataSnapshot photoSnapshot : photosSnapshot.getChildren()) {
                        FirebasePhoto firebasePhoto = photoSnapshot.getValue(FirebasePhoto.class);
                        //On passe a notre photo son id et l'url
                        Photo photo = new Photo(photoSnapshot.getKey(), firebasePhoto.userId, firebasePhoto.url, firebasePhoto.usersWhoLike, firebasePhoto.usersWhoLike, firebasePhoto.date);
                        photos.add(photo);
                    }

                    //On trie la liste
                    Collections.sort(photos, new PhotoNbLikesComparator());
                    themeRecyclerView.redraw(photos);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void updateVotePhotos(final Activity activity, final Context context, final SwipePlaceHolderView swipePlaceHolderView) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        //TODO probleme ici car vote theme null (pas tres grave)
        String voteThemeId = CurrentPostAndVoteTheme.getInstance().getVoteTheme().getThemeId();
        Query query = myRef.orderByChild(THEMEID_COLUMN).equalTo(voteThemeId);
        final String userId = FirebaseHandler.getFirebaseUser().getUid();

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot photosSnapshot) {
                ArrayList<Photo> photos = new ArrayList<>();
                if(photosSnapshot.exists()){
                    for (DataSnapshot photoSnapshot : photosSnapshot.getChildren()) {
                        FirebasePhoto firebasePhoto = photoSnapshot.getValue(FirebasePhoto.class);

                        Photo photo = new Photo(photoSnapshot.getKey(), firebasePhoto.userId, firebasePhoto.url, firebasePhoto.usersWhoLike, firebasePhoto.usersWhoDislike,  firebasePhoto.date);
                        //On retire les photos pour lesquelles on a déjà voté
                        if(photo.userNeverVote(userId)){
//                            photo.calculateNbVotes();
                            //On passe a notre photo son id et l'url
                            photos.add(photo);
                        }
                    }
                }

                Collections.shuffle(photos);

                for (Photo photo : photos) {
                    swipePlaceHolderView.addView(new PhotoCard(activity, context, photo, swipePlaceHolderView));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Update les photos de l'utilsateur dans son profil
    //todo
    public static void updatePhotosForCurrentUser(final ProfilPhotosRecyclerView profilPhotosRecyclerView) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        Query query = myRef.orderByChild(USERID_COLUMN).equalTo(MyFirebaseUser.getFirebaseUserUUID());

        //Before  addValueEventListener
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot photosSnapshot) {
                if(photosSnapshot.exists()){
                    ArrayList<Photo> photos = new ArrayList<>();

                    for (DataSnapshot photoSnapshot : photosSnapshot.getChildren()) {
                        FirebasePhoto firebasePhoto = photoSnapshot.getValue(FirebasePhoto.class);
                        //On passe a notre photo son id et l'url
                        Photo photo = new Photo(photoSnapshot.getKey(), firebasePhoto.userId, firebasePhoto.url, firebasePhoto.usersWhoLike, firebasePhoto.usersWhoDislike, firebasePhoto.date);
                        photos.add(photo);
                    }

                    Collections.sort(photos, new PhotoDateComparator());
//                    profilPhotosRecyclerViewAdapter.update(photos);
                    profilPhotosRecyclerView.redraw(photos);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Update les photos de l'utilsateur dans son profil
    public static void updatePhotosForUser(String userId, final ConsultProfilRecyclerView consultProfilRecyclerView) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        Query query = myRef.orderByChild(USERID_COLUMN).equalTo(userId);

        //Before  addValueEventListener
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot photosSnapshot) {
                if(photosSnapshot.exists()){
                    ArrayList<Photo> photos = new ArrayList<>();

                    for (DataSnapshot photoSnapshot : photosSnapshot.getChildren()) {
                        FirebasePhoto firebasePhoto = photoSnapshot.getValue(FirebasePhoto.class);
                        //On passe a notre photo son id et l'url
                        Photo photo = new Photo(photoSnapshot.getKey(), firebasePhoto.userId, firebasePhoto.url, firebasePhoto.usersWhoLike, firebasePhoto.usersWhoDislike, firebasePhoto.date);
                        photos.add(photo);
                    }

                    Collections.sort(photos, new PhotoDateComparator());
//                    consultProfilAdapter.update(photos);
                    consultProfilRecyclerView.redraw(photos);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Compte le nombre de photos postées par l'utilisateur et le nombres de likes total
    public static void updatePhotosAndLikesCountForUser(String userId, final TextView nbLikesTV, final TextView nbPhotosTV) {
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
        Query query = myRef.orderByChild(USERID_COLUMN).equalTo(userId);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot photosSnapshot) {
                if(photosSnapshot.exists()){
                    long nbPhotos = photosSnapshot.getChildrenCount();
                    nbPhotosTV.setText(Long.toString(nbPhotos));
                    long nbLikes = 0;

                    for (DataSnapshot photoSnapshot : photosSnapshot.getChildren()) {
                        FirebasePhoto firebasePhoto = photoSnapshot.getValue(FirebasePhoto.class);
//                        DataSnapshot userWhoLike = ;
                        nbLikes += firebasePhoto.usersWhoLike.size();
                    }
                    nbLikesTV.setText(Long.toString(nbLikes));
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Compte le nombre de photos postées par l'utilisateur et le nombres de likes total
    public static void updatePhotoCountForClassement(final Context context, final ClassementRecyclerView classementRecyclerView, final RelativeLayout parentPodiumRL) {
        final DatabaseReference ref = FirebaseHandler.getFirebaseDatabase().getReference(PHOTOS_COLUMN);
//        Query query = myRef.orderByChild(USERID_COLUMN).equalTo(userId);

        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot photosSnapshot) {
                if(photosSnapshot.exists()){
                    HashMap<String, Integer> hashUserIdNbLikes = new HashMap<>();

                    for (DataSnapshot photoSnapshot : photosSnapshot.getChildren()) {
                        FirebasePhoto firebasePhoto = photoSnapshot.getValue(FirebasePhoto.class);
                        int currentNbLikes = 0;

                        if(hashUserIdNbLikes.get(firebasePhoto.userId) != null)
                            currentNbLikes = hashUserIdNbLikes.get(firebasePhoto.userId);

                        hashUserIdNbLikes.put(firebasePhoto.userId, currentNbLikes + firebasePhoto.usersWhoLike.size());
                    }

                    MyFirebaseUser.updateClassement(hashUserIdNbLikes, context, classementRecyclerView, parentPodiumRL);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
