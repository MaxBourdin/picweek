package com.maxapp.picweek.firebasemodel;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maxapp.picweek.activity.archives.theme.OverlayView;
import com.maxapp.picweek.activity.community.classement.ClassementRecyclerView;
import com.maxapp.picweek.model.Classement;
import com.maxapp.picweek.model.User;
import com.maxapp.picweek.tools.FirebaseHandler;
import com.maxapp.picweek.tools.UserNbLikesComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by bourdin on 24/01/18
 */

public class MyFirebaseUser {

    public final static String USERS_COLUMN = "users";
    public final static String FIREBASEID_COLUMN = "firebaseId";
    public final static String FACEBOOKID_COLUMN = "facebookId";
    public final static String GOOGLEID_COLUMN = "googleId";
    public String firebaseId, facebookId, googleId, userName;

    public MyFirebaseUser() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public MyFirebaseUser(String firebaseId, String facebookId, String googleId, String userName) {
        this.firebaseId = firebaseId;
        this.facebookId = facebookId;
        this.googleId = googleId;
        this.userName = userName;
    }

    public static String getCurrentFacebookUserId(){
        String facebookUserId = "";
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        // find the Facebook profile and get the user's id
        for(UserInfo profile : user.getProviderData()) {
            // check if the provider id matches "facebook.com"
            if(FacebookAuthProvider.PROVIDER_ID.equals(profile.getProviderId())) {
                facebookUserId = profile.getUid();
            }
        }

        return facebookUserId;
    }

    public static void writeNewTestUser(final String userName, final String uid, final String facebookId){
        final FirebaseUser user = FirebaseHandler.getFirebaseUser();
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(USERS_COLUMN);
        final String currentFacebookUserId = getCurrentFacebookUserId();

        Query query = myRef.orderByChild(FACEBOOKID_COLUMN).equalTo(currentFacebookUserId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                MyFirebaseUser myFirebaseUser = new MyFirebaseUser(uid, facebookId, "0", userName);
                myRef.push().setValue(myFirebaseUser);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public static void writeNewUser(final String userName){
        final FirebaseUser user = FirebaseHandler.getFirebaseUser();
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(USERS_COLUMN);
        final String currentFacebookUserId = getCurrentFacebookUserId();

        Query query = myRef.orderByChild(FACEBOOKID_COLUMN).equalTo(currentFacebookUserId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!userExist(dataSnapshot))
                    addUserToFirebase();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

            private boolean userExist(DataSnapshot dataSnapshot){
                //Si aucun user enregistré dans la bdd
                if(dataSnapshot.getChildrenCount() == 0){
                    return false;
                }

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()){
                    MyFirebaseUser myFirebaseUser = childDataSnapshot.getValue(MyFirebaseUser.class);
                    //Si un user dans la bdd a déjà le meme uid
                    if(myFirebaseUser.facebookId.equals(currentFacebookUserId)){
                        return true;
                    }
                }

                return false;
            }

            private void addUserToFirebase(){
//                HashMap<String, String> hashMap = new HashMap<>();
//                hashMap.put(FACEBOOKID_COLUMN, user.getUid());
//                hashMap.put(GOOGLEID_COLUMN, "0");

                MyFirebaseUser myFirebaseUser = new MyFirebaseUser(user.getUid(), currentFacebookUserId, "0", userName);
                myRef.push().setValue(myFirebaseUser);
            }
        });
    }

    public static String getSmallProfilePhotoURL(){
        return FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString();
    }

    public static String getLargeProfilePhotoURL(String facebookId){
        String photoUrl = "https://graph.facebook.com/" + facebookId + "/picture?height=250";
        return photoUrl;
    }

    public static void updateOverlayNameForUser(String userId, final OverlayView overlayView){
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(USERS_COLUMN);
        Query query = myRef.orderByChild(FIREBASEID_COLUMN).equalTo(userId);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot usersSnapshot) {
                if(usersSnapshot.exists()){
                    for (DataSnapshot userSnapshot : usersSnapshot.getChildren()) {
                        MyFirebaseUser myFirebaseUser = userSnapshot.getValue(MyFirebaseUser.class);

                        overlayView.setUserPseudo(myFirebaseUser.userName);
                        overlayView.setUserId(myFirebaseUser.firebaseId);
                    }
                }else{
                    //n'arrive jamais
                    overlayView.setUserPseudo("");
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void updateProfilInformations(String userId, final TextView pseudoTV, final ImageView profilIV){
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(USERS_COLUMN);
        Query query = myRef.orderByChild(FIREBASEID_COLUMN).equalTo(userId);

        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot usersSnapshot) {
                if(usersSnapshot.exists()) {
                    for (DataSnapshot userSnapshot : usersSnapshot.getChildren()) {
                        MyFirebaseUser myFirebaseUser = userSnapshot.getValue(MyFirebaseUser.class);

                        pseudoTV.setText(myFirebaseUser.userName);
                        String photoUrl = getLargeProfilePhotoURL(myFirebaseUser.facebookId);
                        Glide.with(pseudoTV.getContext()).load(photoUrl).into(profilIV);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void updateClassement(final HashMap<String, Integer> hashUserIdNbLikes, final Context context, final ClassementRecyclerView classementRecyclerView, final RelativeLayout parentPodiumRL){
        final DatabaseReference myRef = FirebaseHandler.getFirebaseDatabase().getReference(USERS_COLUMN);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    ArrayList<User> users = new ArrayList<>();

                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        MyFirebaseUser myFirebaseUser = childSnapshot.getValue(MyFirebaseUser.class);
                        String photoUrl = getLargeProfilePhotoURL(myFirebaseUser.facebookId);

                        int nbLikes = 0;
                        if(hashUserIdNbLikes.get(myFirebaseUser.firebaseId) != null)
                            nbLikes = hashUserIdNbLikes.get(myFirebaseUser.firebaseId);

                        User user = new User(myFirebaseUser.firebaseId, myFirebaseUser.userName, photoUrl, nbLikes);
                        users.add(user);
                    }

                    Collections.sort(users, new UserNbLikesComparator());

                    Classement classement = new Classement(context, users);
                    classementRecyclerView.init(users);
//                    classementRecyclerView.updateAdapter(classement);
                    parentPodiumRL.addView(classement.getPodium().getMainLinearLayout());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static String getName(){
        return FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
    }

    public static String getFirebaseUserUUID(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
