package com.maxapp.picweek.firebasemodel;

import com.maxapp.picweek.model.Theme;

/**
 * Created by bourdin on 28/01/18
 */

public class CurrentPostAndVoteTheme {
    public Theme postTheme = new Theme();
    public Theme voteTheme= new Theme();

    /** Constructeur privé */
    private CurrentPostAndVoteTheme() {
    }

    /** Instance unique non préinitialisée */
    private static CurrentPostAndVoteTheme INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static CurrentPostAndVoteTheme getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CurrentPostAndVoteTheme();
        }
        return INSTANCE;
    }

    public Theme getPostTheme() {
        return postTheme;
    }

    public void setPostTheme(Theme postTheme) {
        this.postTheme = postTheme;
    }

    public Theme getVoteTheme() {
        return voteTheme;
    }

    public void setVoteTheme(Theme voteTheme) {
        this.voteTheme = voteTheme;
    }

    public static void setINSTANCE(CurrentPostAndVoteTheme INSTANCE) {
        CurrentPostAndVoteTheme.INSTANCE = INSTANCE;
    }
}

