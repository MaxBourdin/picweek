package com.maxapp.picweek.firebasemodel;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.maxapp.picweek.dialog.SuccessUploadDialog;
import com.maxapp.picweek.dialog.UnSuccessUploadDialog;

import java.io.File;

/**
 * Created by bourdin on 31/01/18
 */

public class MyFirebaseStorage {
    public static void addToFirebase(final Context context, String filePath, String cameraOrGallery){
        FirebaseStorage storage = FirebaseStorage.getInstance();

        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();

        Uri file = Uri.fromFile(new File(filePath));
        StorageReference riversRef = storageRef.child("images/" + cameraOrGallery + "/" + file.getLastPathSegment());

        UploadTask uploadTask = riversRef.putFile(file);

        // Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                new UnSuccessUploadDialog(context);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                FirebasePhoto.writeNewPhoto(taskSnapshot.getDownloadUrl().toString());
                new SuccessUploadDialog(context);
            }
        });
    }
}
